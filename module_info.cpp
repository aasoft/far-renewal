/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "module_info.h"
#include "string_rc.h"


void module_info::view(const module& mod)
{
	const wstring vers = mod.ver_current.text();
	const wstring guid = mod.guid_text();

	const wchar_t* state = nullptr;
	if (!mod.loaded)
		state = _PSI.GetMsg(&_FPG, ps_mi_state_unloaded);
	else {
		const bool cached = mod.is_far() || mod.is_self() ? false : GetModuleHandle(_FSF.PointToName(mod.path.c_str())) == nullptr;
		state = _PSI.GetMsg(&_FPG, cached ? ps_mi_state_cached : ps_mi_state_active);
	}

	const FarDialogItem dlg_items[] = {
		/*  0 */ { DI_DOUBLEBOX,  3,  1, 70, 13, 0, nullptr, nullptr, DIF_NONE, mod.name.c_str() },
		/*  1 */ { DI_TEXT,       5,  2, 20,  2, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_mi_name) },
		/*  2 */ { DI_EDIT,      20,  2, 68,  2, 0, nullptr, nullptr, DIF_READONLY | DIF_SELECTONENTRY, mod.name.c_str() },
		/*  3 */ { DI_TEXT,       5,  3, 20,  3, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_mi_description) },
		/*  4 */ { DI_EDIT,      20,  3, 68,  3, 0, nullptr, nullptr, DIF_READONLY | DIF_SELECTONENTRY, mod.description.c_str() },
		/*  5 */ { DI_TEXT,       5,  4, 20,  4, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_mi_author) },
		/*  6 */ { DI_EDIT,      20,  4, 68,  4, 0, nullptr, nullptr, DIF_READONLY | DIF_SELECTONENTRY, mod.author.c_str() },
		/*  7 */ { DI_TEXT,       5,  5, 20,  5, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_me_home_page) },
		/*  8 */ { DI_EDIT,      20,  5, 68,  5, 0, nullptr, nullptr, DIF_READONLY | DIF_SELECTONENTRY, mod.home_page_url.c_str() },
		/*  9 */ { DI_TEXT,       5,  6, 20,  6, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_me_changelog) },
		/* 10 */ { DI_EDIT,      20,  6, 68,  6, 0, nullptr, nullptr, DIF_READONLY | DIF_SELECTONENTRY, mod.changelog_url.c_str() },
		/* 11 */ { DI_TEXT,       5,  7, 20,  7, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_mi_path) },
		/* 12 */ { DI_EDIT,      20,  7, 68,  7, 0, nullptr, nullptr, DIF_READONLY | DIF_SELECTONENTRY, mod.path.c_str() },
		/* 13 */ { DI_TEXT,       5,  8, 20,  8, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_mi_guid) },
		/* 14 */ { DI_EDIT,      20,  8, 68,  8, 0, nullptr, nullptr, DIF_READONLY | DIF_SELECTONENTRY, guid.c_str() },
		/* 15 */ { DI_TEXT,       5,  9, 20,  9, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_mi_version) },
		/* 16 */ { DI_EDIT,      20,  9, 68,  9, 0, nullptr, nullptr, DIF_READONLY | DIF_SELECTONENTRY, vers.c_str() },
		/* 17 */ { DI_TEXT,       5, 10, 20, 10, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_mi_state) },
		/* 18 */ { DI_EDIT,      20, 10, 68, 10, 0, nullptr, nullptr, DIF_READONLY | DIF_SELECTONENTRY, state },
		/* 19 */ { DI_TEXT,       0, 11,  0, 11, 0, nullptr, nullptr, DIF_SEPARATOR },
		/* 20 */ { DI_BUTTON,     0, 12,  0, 12, 0, nullptr, nullptr, DIF_CENTERGROUP | DIF_DEFAULTBUTTON | DIF_FOCUS, _PSI.GetMsg(&_FPG, ps_close) }
	};

	HANDLE dlg = _PSI.DialogInit(&_FPG, &_FPG, -1, -1, 74, 15, nullptr, dlg_items, sizeof(dlg_items) / sizeof(dlg_items[0]), 0, FDLG_NONE, nullptr, nullptr);
	_PSI.DialogRun(dlg);
	_PSI.DialogFree(dlg);
}
