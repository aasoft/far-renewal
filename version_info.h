/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "common.h"


struct version_info
{
	version_info() : major(0), minor(0), revision(0), build(0) {}
	version_info(const VersionInfo& vi) : major(vi.Major), minor(vi.Minor), revision(vi.Revision), build(vi.Build) {}

	/**
	 * Check for version valid.
	 * \return true if version info is valid
	 */
	bool valid() const
	{
		return (major != 0 || minor != 0 || revision != 0 || build != 0);
	}

	/**
	 * Reset version info.
	 */
	void reset()
	{
		major = minor = revision = build = 0;
	}

	/**
	 * Convert version info to text presentation.
	 * \return version info as text
	 */
	wstring text() const
	{
		if (!valid())
			return L"n/a";
		wchar_t fmt[16];
		wcscpy_s(fmt, L"%d.%d");
		if (build)
			wcscat_s(fmt, L".%d.%d");
		else if (revision)
			wcscat_s(fmt, L".%d");
		wchar_t buf[128];
		swprintf_s(buf, fmt, major, minor, revision, build);
		return buf;
	}

	//Compare operator.
	bool operator > (const version_info& other) const
	{
		return
			(major > other.major) ||
			(major == other.major && minor > other.minor) ||
			(major == other.major && minor == other.minor && revision > other.revision) ||
			(major == other.major && minor == other.minor && revision == other.revision && build > other.build);
	}

	intptr_t major;    ///< Major version number
	intptr_t minor;    ///< Minor version number
	intptr_t revision; ///< Revision version number
	intptr_t build;    ///< Build version number
};
