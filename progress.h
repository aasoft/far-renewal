/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "common.h"


class progress
{
public:
	/**
	 * Constructor.
	 * \param title window title
	 * \param modules_count total modules count
	 */
	progress(const wchar_t* title, const size_t modules_count);

	~progress();

	/**
	 * Update progress value.
	 * \param current_mod_name currently handled module name
	 */
	void update(const wchar_t* current_mod_name = nullptr);

	/**
	 * Check for abort request.
	 * \return true if user requested abort
	 */
	static bool aborted();

private:
	const wchar_t* _title;          ///< Window title
	const wchar_t* _curr_mod;       ///< Currently handled module name
	size_t         _curr_module;    ///< Currently handled module number
	size_t         _modules_count;  ///< Total modules count
};
