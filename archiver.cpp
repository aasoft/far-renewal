/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "archiver.h"
#include "7z\CPP\7zip\Archive\IArchive.h"
#include "7z\CPP\7zip\IPassword.h"
#include <atlbase.h>
#include <shlwapi.h>
#include <shellapi.h>
#include <shlobj.h>
#include <comdef.h>


//7zip GUIDs
const GUID IID_ISequentialInStream =     { 0x23170F69, 0x40C1, 0x278A, { 0x00, 0x00, 0x00, 0x03, 0x00, 0x01, 0x00, 0x00 } };
const GUID IID_IInStream =               { 0x23170F69, 0x40C1, 0x278A, { 0x00, 0x00, 0x00, 0x03, 0x00, 0x03, 0x00, 0x00 } };
const GUID IID_ISequentialOutStream =    { 0x23170F69, 0x40C1, 0x278A, { 0x00, 0x00, 0x00, 0x03, 0x00, 0x02, 0x00, 0x00 } };
const GUID IID_IStreamGetSize =          { 0x23170F69, 0x40C1, 0x278A, { 0x00, 0x00, 0x00, 0x03, 0x00, 0x06, 0x00, 0x00 } };
const GUID IID_IOutStream =              { 0x23170F69, 0x40C1, 0x278A, { 0x00, 0x00, 0x00, 0x03, 0x00, 0x04, 0x00, 0x00 } };
const GUID IID_ICryptoGetTextPassword =  { 0x23170F69, 0x40C1, 0x278A, { 0x00, 0x00, 0x00, 0x05, 0x00, 0x10, 0x00, 0x00 } };
const GUID IID_IArchiveExtractCallback = { 0x23170F69, 0x40C1, 0x278A, { 0x00, 0x00, 0x00, 0x06, 0x00, 0x20, 0x00, 0x00 } };
const GUID IID_IInArchive =	             { 0x23170F69, 0x40C1, 0x278A, { 0x00, 0x00, 0x00, 0x06, 0x00, 0x60, 0x00, 0x00 } };
const GUID CLSID_CFormat7z =             { 0x23170F69, 0x40C1, 0x278A, { 0x10, 0x00, 0x00, 0x01, 0x10, 0x07, 0x00, 0x00 } };
const GUID CLSID_CFormatRar =            { 0x23170F69, 0x40C1, 0x278A, { 0x10, 0x00, 0x00, 0x01, 0x10, 0x03, 0x00, 0x00 } };
const GUID CLSID_CFormatZip =            { 0x23170F69, 0x40C1, 0x278A, { 0x10, 0x00, 0x00, 0x01, 0x10, 0x01, 0x00, 0x00 } };


/** PROPVARIANT wrapper */
class prop_variant : public PROPVARIANT
{
public:
	prop_variant()	{ vt = VT_EMPTY; wReserved1 = 0; }
	~prop_variant()	{ clear(); }
	HRESULT clear() { return VariantClear(reinterpret_cast<VARIANTARG*>(this)); }
};

/** IInStream/IStreamGetSize wrapper */
class IInStreamWrapper : public IInStream, public IStreamGetSize
{
public:
	IInStreamWrapper(CComPtr<IStream> base_stream)
		: _ref_count(0), _base_stream(base_stream)
	{
	}

	//From IUnknown
	STDMETHOD_(ULONG, AddRef)()
	{
		return static_cast<ULONG>(InterlockedIncrement(&_ref_count));
	}

	//From IUnknown
	STDMETHOD_(ULONG, Release)()
	{
		const ULONG cnt = static_cast<ULONG>(InterlockedDecrement(&_ref_count));
		if (cnt == 0)
			delete this;
		return cnt;
	}

	//From IUnknown
	STDMETHOD(QueryInterface)(REFIID iid, void** ppv_object)
	{
		if (iid == __uuidof(IUnknown) ||
			iid == IID_ISequentialInStream ||
			iid == IID_IInStream ||
			iid == IID_IStreamGetSize) {
				*ppv_object = this;
				AddRef();
				return S_OK;
		}
		return E_NOINTERFACE;
	}

	//From ISequentialInStream
	STDMETHOD(Read)(void* data, UInt32 size, UInt32* processed_size)
	{
		ULONG read = 0;
		const HRESULT hr = _base_stream->Read(data, size, &read);
		if (processed_size)
			*processed_size = read;
		return SUCCEEDED(hr) ? S_OK : hr;	// Transform S_FALSE to S_OK
	}

	//From IInStream
	STDMETHOD(Seek)(Int64 offset, UInt32 seek_origin, UInt64* new_position)
	{
		LARGE_INTEGER move;
		ULARGE_INTEGER new_pos;
		move.QuadPart = offset;
		const HRESULT hr = _base_stream->Seek(move, seek_origin, &new_pos);
		if (new_position)
			*new_position =  new_pos.QuadPart;
		return hr;
	}

	//From IStreamGetSize
	STDMETHOD(GetSize)(UInt64* size)
	{
		STATSTG stat;
		const HRESULT hr = _base_stream->Stat(&stat, STATFLAG_NONAME);
		if (SUCCEEDED(hr))
			*size = stat.cbSize.QuadPart;
		return hr;
	}

private:
	LONG				_ref_count;
	CComPtr<IStream>	_base_stream;
};

/** IOutStream wrapper */
class IOutStreamWrapper : public IOutStream
{
public:
	IOutStreamWrapper(CComPtr<IStream> base_stream)
		: _ref_count(0), _base_stream(base_stream)
	{
	}

	//From IUnknown
	STDMETHOD_(ULONG, AddRef)()
	{
		return static_cast<ULONG>(InterlockedIncrement(&_ref_count));
	}

	//From IUnknown
	STDMETHOD_(ULONG, Release)()
	{
		const ULONG cnt = static_cast<ULONG>(InterlockedDecrement(&_ref_count));
		if (cnt == 0)
			delete this;
		return cnt;
	}

	//From IUnknown
	STDMETHOD(QueryInterface)(REFIID iid, void** ppv_object)
	{
		if (iid == __uuidof(IUnknown) ||
			iid == IID_ISequentialOutStream ||
			iid == IID_IOutStream) {
				*ppv_object = this;
				AddRef();
				return S_OK;
		}
		return E_NOINTERFACE;
	}

	//From ISequentialOutStream
	STDMETHOD(Write)(const void* data, UInt32 size, UInt32* processed_size)
	{
		ULONG written = 0;
		const HRESULT hr = _base_stream->Write(data, size, &written);
		if (processed_size)
			*processed_size = written;
		return hr;
	}

	//From IOutStream
	STDMETHOD(Seek)(Int64 offset, UInt32 seek_origin, UInt64* new_position)
	{
		LARGE_INTEGER move;
		ULARGE_INTEGER new_pos;
		move.QuadPart = offset;
		HRESULT hr = _base_stream->Seek(move, seek_origin, &new_pos);
		if (new_position)
			*new_position =  new_pos.QuadPart;
		return hr;
	}

	//From IOutStream
	STDMETHOD(SetSize)(UInt64 new_size)
	{
		ULARGE_INTEGER size;
		size.QuadPart = new_size;
		return _base_stream->SetSize(size);
	}

private:
	LONG				_ref_count;
	CComPtr<IStream>	_base_stream;
};

/** IArchiveExtractCallback/ICryptoGetTextPassword wrapper */
class IArchiveExtractCallbackWrapper : public IArchiveExtractCallback, public ICryptoGetTextPassword
{
public:
	IArchiveExtractCallbackWrapper(CComPtr<IInArchive> arch, const wchar_t* out_directory)
		: _ref_count(0), _archive(arch), _out_directory(out_directory), _attrib(static_cast<DWORD>(-1))
	{
		_modified_time.dwLowDateTime = _modified_time.dwHighDateTime = 0;
	}

	//From IUnknown
	STDMETHOD_(ULONG, AddRef)()
	{
		return static_cast<ULONG>(InterlockedIncrement(&_ref_count));
	}

	//From IUnknown
	STDMETHOD_(ULONG, Release)()
	{
		const ULONG cnt = static_cast<ULONG>(InterlockedDecrement(&_ref_count));
		if (cnt == 0)
			delete this;
		return cnt;
	}

	//From IUnknown
	STDMETHOD(QueryInterface)(REFIID iid, void** ppv_object)
	{
		if (iid == __uuidof(IUnknown) ||
			iid == IID_IArchiveExtractCallback ||
			iid == IID_ICryptoGetTextPassword) {
				*ppv_object = this;
				AddRef();
				return S_OK;
		}
		return E_NOINTERFACE;
	}

	//From IProgress
	STDMETHOD(SetTotal)(UInt64)				{ return S_OK; }
	STDMETHOD(SetCompleted)(const UInt64*)	{ return S_OK; }

	//From IArchiveExtractCallback
	STDMETHOD(GetStream)(UInt32 index, ISequentialOutStream** out_stream, Int32 ask_extract_mode)
	{
		HRESULT hr = S_OK;

		//Get relative file path
		prop_variant prop_rel_path;
		hr = _archive->GetProperty(index, kpidPath, &prop_rel_path);
		if (hr != S_OK)
			return hr;
		if (prop_rel_path.vt != VT_BSTR)
			return E_FAIL;
		const wstring relative_path = prop_rel_path.bstrVal;

		if (ask_extract_mode != NArchive::NExtract::NAskMode::kExtract)
			return S_OK;	//Nothing to do

		//Get file attributes
		_attrib = static_cast<DWORD>(-1);
		prop_variant prop_attr;
		hr = _archive->GetProperty(index, kpidAttrib, &prop_attr);
		if (hr != S_OK)
			return hr;
		if (prop_attr.vt == VT_UI4)
			_attrib = prop_attr.ulVal;

		//Get file modification time
		_modified_time.dwLowDateTime = _modified_time.dwHighDateTime = 0;
		prop_variant prop_mt;
		hr = _archive->GetProperty(index, kpidMTime, &prop_mt);
		if (hr != S_OK)
			return hr;
		if (prop_mt.vt == VT_FILETIME)
			_modified_time = prop_mt.filetime;

		//Set absolute file path
		_absolute_path = _out_directory;
		if (*_absolute_path.rbegin() != L'/' && *_absolute_path.rbegin() != L'\\')
			_absolute_path += L'\\';
		_absolute_path += relative_path;

		//Check for directory item
		prop_variant prop_is_dir;
		hr = _archive->GetProperty(index, kpidIsDir, &prop_is_dir);
		if (hr != S_OK)
			return hr;
		if (prop_is_dir.vt == VT_BOOL && prop_is_dir.boolVal != VARIANT_FALSE) {
			//Creating the directory structure
			SHCreateDirectoryEx(nullptr, _absolute_path.c_str(), nullptr);
			*out_stream = nullptr;
			return S_OK;
		}

		//Create directory for file item
		wstring dir_path = _absolute_path;
		const size_t path_delim = dir_path.find_last_of(L"\\/");
		if (path_delim != string::npos)
			dir_path.erase(path_delim);
		SHCreateDirectoryEx(nullptr, dir_path.c_str(), nullptr);

		//Open write stream
		CComPtr<IStream> write_stream;
		hr = SHCreateStreamOnFileEx(_absolute_path.c_str(), STGM_CREATE | STGM_WRITE, FILE_ATTRIBUTE_NORMAL, TRUE, nullptr, &write_stream);
		if (hr != S_OK)
			return hr;

		//Set out stream
		CComPtr<IOutStreamWrapper> os = new IOutStreamWrapper(write_stream);
		*out_stream = os.Detach();

		return hr;
	}

	//From IArchiveExtractCallback
	STDMETHOD(PrepareOperation)(Int32) { return S_OK; }

	//From IArchiveExtractCallback
	STDMETHOD(SetOperationResult)(Int32)
	{
		if (_absolute_path.empty() )
			return S_OK;
		if (_attrib != static_cast<DWORD>(-1))
			SetFileAttributes(_absolute_path.c_str(), _attrib);
		if (_modified_time.dwHighDateTime || _modified_time.dwLowDateTime) {
			const HANDLE file = CreateFile(_absolute_path.c_str(), GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			if (file != INVALID_HANDLE_VALUE) {
				SetFileTime(file, nullptr, nullptr, &_modified_time);
				CloseHandle(file);
			}
		}
		return S_OK;
	}

	//From ICryptoGetTextPassword
	STDMETHOD(CryptoGetTextPassword)(BSTR*) { return E_ABORT; }

private:
	LONG				_ref_count;
	CComPtr<IInArchive>	_archive;
	const wchar_t*		_out_directory;

	FILETIME	_modified_time;
	DWORD		_attrib;
	wstring		_absolute_path;
};


archiver::archiver()
:	_7z_lib(nullptr),
	_create_obj_fx(nullptr)
{
}


archiver::~archiver()
{
	if (_7z_lib)
		FreeLibrary(_7z_lib);
}


bool archiver::extract(const vector<unsigned char>& data, const wchar_t* dst_path)
{
	assert(!data.empty());
	assert(dst_path && *dst_path);

	if (!load_7zlib())
		return false;

	if (data.size() < 16) {
		set_last_error(L"Archive corrupted");
		return false;
	}

	//Get archive format
	const GUID* guid_format = nullptr;
	if (data[0] == 'P' && data[1] == 'K')
		guid_format = &CLSID_CFormatZip;
	else if (data[0] == 'R' && data[1] == 'a' && data[2] == 'r')
		guid_format = &CLSID_CFormatRar;
	else if (data[0] == '7' && data[1] == 'z')
		guid_format = &CLSID_CFormat7z;
	else {
		//May be it's a rar format?
		const unsigned char rar_header[] = { 0x52, 0x61, 0x72, 0x21, 0x1A, 0x07, 0x00 };
		for (size_t i = 0; i < 64 && guid_format == nullptr; ++i) {
			if (memcmp(rar_header, &data.front() + i, sizeof(rar_header) / sizeof(rar_header[0])) == 0)
				guid_format = &CLSID_CFormatRar;
		}
	}

	if (guid_format == nullptr) {
		set_last_error(L"Unsupported archive format");
		return false;
	}

	HRESULT hr;

	CComPtr<IInArchive> in_archive;
	hr = _create_obj_fx(guid_format, &IID_IInArchive, reinterpret_cast<void**>(&in_archive));
	if (hr != S_OK) {
		set_last_error(L"Unable to create archive object", HRESULT_CODE(hr));
		return false;
	}

	CComPtr<IStream> mem_stream = SHCreateMemStream(&data.front(), static_cast<UINT>(data.size()));
	CComPtr<IInStreamWrapper> in_stream = new IInStreamWrapper(mem_stream);

	hr = in_archive->Open(in_stream, 0, nullptr);
	if (hr != S_OK) {
		set_last_error(L"Unable to open input archive stream", HRESULT_CODE(hr));
		return false;
	}

	CComPtr<IArchiveExtractCallbackWrapper> extract_cb = new IArchiveExtractCallbackWrapper(in_archive, dst_path);
	hr = in_archive->Extract(nullptr, static_cast<UInt32>(-1), false, extract_cb);
	if (hr != S_OK) {
		set_last_error(L"Unable to extract archive", HRESULT_CODE(hr));
		return false;
	}

	return true;
}


bool archiver::load_7zlib()
{
	if (_7z_lib)
		return true;	//Already loaded

	//Load 7z.dll library
	const wchar_t* lib_name = L"7z.dll";
	_7z_lib = LoadLibrary(lib_name);
	if (!_7z_lib) {
		wstring path = get_7z_path(HKEY_CURRENT_USER);
		path += lib_name;
		_7z_lib = LoadLibrary(path.c_str());
		if (!_7z_lib) {
			path = get_7z_path(HKEY_LOCAL_MACHINE);
			path += lib_name;
			_7z_lib = LoadLibrary(path.c_str());
			if (!_7z_lib) {
				GUID arclite_guid = { 0x65642111, 0xAA69, 0x4B84, { 0xB4, 0xB8, 0x92, 0x49, 0x57, 0x9E, 0xC4, 0xFA } };
				HANDLE arclite_plugin = reinterpret_cast<HANDLE>(_PSI.PluginsControl(nullptr, PCTL_FINDPLUGIN, PFM_GUID, &arclite_guid));
				if (arclite_plugin) {
					const intptr_t pi_len = _PSI.PluginsControl(arclite_plugin, PCTL_GETPLUGININFORMATION, 0, nullptr);
					if (pi_len) {
						vector<unsigned char> pi_buff(pi_len);
						FarGetPluginInformation* pi = reinterpret_cast<FarGetPluginInformation*>(&pi_buff.front());
						pi->StructSize = sizeof(FarGetPluginInformation);
						if (_PSI.PluginsControl(arclite_plugin, PCTL_GETPLUGININFORMATION, pi_len, pi)) {
							path = pi->ModuleName;
							const size_t path_delim = path.find_last_of(L"\\/");
							if (path_delim != string::npos)
								path.erase(path_delim + 1);
							path += lib_name;
							_7z_lib = LoadLibrary(path.c_str());
						}
					}
				}
			}
		}
	}

	if (!_7z_lib) {
		set_last_error(L"7z.dll library not found or cannot be loaded");
		return false;
	}

	//Find CreateObject function
	assert(_create_obj_fx == nullptr);
	_create_obj_fx = reinterpret_cast<CreateObjectFx>(GetProcAddress(_7z_lib, "CreateObject"));
	if (!_create_obj_fx) {
		set_last_error(L"CreateObject function not found in the 7z.dll library");
		FreeLibrary(_7z_lib);
		_7z_lib = nullptr;
		return false;
	}

	return true;
}


wstring archiver::get_7z_path(const HKEY key) const
{
	wstring path;

	HKEY reg_key;
	if (RegOpenKeyEx(key, L"Software\\7-Zip", 0, KEY_READ, &reg_key) == ERROR_SUCCESS) {
		DWORD data_len = 0;
		if (RegQueryValueEx(reg_key, L"Path", nullptr, nullptr, nullptr, &data_len) != ERROR_SUCCESS) {
			RegCloseKey(reg_key);
			return wstring();
		}
		path.resize(data_len / sizeof(wchar_t));
		if (RegQueryValueEx(reg_key, L"Path", nullptr, nullptr, reinterpret_cast<LPBYTE>(&path.front()), &data_len) != ERROR_SUCCESS)
			path.clear();
		else {
			while(!path.empty() && *path.rbegin() == 0)
				path.erase(path.length() - 1);
		}
		RegCloseKey(reg_key);
	}

	return path;
}
