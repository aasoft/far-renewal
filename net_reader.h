/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "common.h"
#include "error_handler.h"
#include <Winhttp.h>


class net_reader : public error_handler
{
public:
	net_reader();
	~net_reader();

	/**
	 * Load content from specified URL.
	 * \param url resource URL
	 * \param content downloaded content
	 * \return false if error
	 */
	bool get_content(const wchar_t* url, vector<unsigned char>& content);

private:
	/**
	 * Free winhttp objects.
	 */
	void free();

	/**
	 * Get http description by code.
	 * \param code response code
	 * \return response description
	 */
	wstring get_code_description(const int code) const;

private:
	HINTERNET _session;   ///< HTTP session
	HINTERNET _connect;   ///< HTTP connection
	HINTERNET _request;   ///< HTTP request

	typedef map< wstring, vector<unsigned char> > page_cache;
	page_cache _cache;    ///< Cached pages
};
