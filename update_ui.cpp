/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "update_ui.h"
#include "version.h"
#include "string_rc.h"
#include "module_update.h"
#include "module_setup.h"
#include "module_info.h"
#include "progress.h"
#include "settings.h"

#define DLGID_LIST             1
#define DLGID_SELECT_MOD_TITLE 3
#define DLGID_SELECT_MOD_SU    4
#define DLGID_SELECT_MOD_IF    5
#define DLGID_SELECT_MOD_HP    6
#define DLGID_SELECT_MOD_CL    7
#define DLGID_UPDATE_CHECK     9
#define DLGID_UPDATE_INSTALL   10
#define DLG_MAX_WIDTH          50


update_ui::update_ui(module_list* mods)
: _mods(mods)
{
	assert(_mods);
	assert(!_mods->empty());
}


update_ui::~update_ui()
{
}


void update_ui::run()
{
	module_list mods = module_list::load_modules();
	if (mods.empty())
		return;

	update_ui upd(&mods);
	upd.show();
}


void update_ui::show()
{
	//Calculate window size
	SMALL_RECT rc_far_wnd;
	_PSI.AdvControl(&_FPG, ACTL_GETFARRECT, 0, &rc_far_wnd);
	intptr_t wnd_height = static_cast<int>(_mods->size()) + 4 + /* Borders */ + 3 /* Module info */ + 2 /* Buttons */;
	const intptr_t max_wnd_height = (rc_far_wnd.Bottom - rc_far_wnd.Top + 1) - 2;
	if (wnd_height > max_wnd_height)
		wnd_height = max_wnd_height;

	//Create modules list
	vector<wstring> plain_view;
	plain_view.resize(_mods->size());
	for (size_t i = 0; i < _mods->size(); ++i)
		plain_view[i] = list_item_text(_mods->at(i));
	vector<FarListItem> far_items;
	far_items.resize(plain_view.size());
	ZeroMemory(&far_items.front(), sizeof(FarListItem) * plain_view.size());
	for (size_t i = 0; i < plain_view.size(); ++i) {
		far_items[i].Text = plain_view[i].c_str();
		far_items[i].Flags = LIF_CHECKED;
	}
	FarList far_list;
	ZeroMemory(&far_list, sizeof(far_list));
	far_list.StructSize = sizeof(far_list);
	far_list.ItemsNumber = far_items.size();
	far_list.Items = &far_items.front();

	//Bug in Far Manager - we can't set new default button (brackets error)
	const wstring btn_chk_label = wstring(L"{ ") + _PSI.GetMsg(&_FPG, ps_check_update) + wstring(L" }");
	const wstring btn_inst_label = wstring(L"[ ") + _PSI.GetMsg(&_FPG, ps_install_update) + wstring(L" ]");

	const FarDialogItem dlg_items[] = {
		/*  0 */ { DI_DOUBLEBOX, 3, 1, 60, wnd_height - 2, 0, nullptr, nullptr, LIF_NONE, _PSI.GetMsg(&_FPG, ps_title) },
		/*  1 */ { DI_LISTBOX,   4, 2, 59, wnd_height - 8, reinterpret_cast<intptr_t>(&far_list), nullptr, nullptr, DIF_LISTNOBOX | DIF_LISTNOCLOSE | DIF_LISTNOAMPERSAND | DIF_FOCUS },
		/*  2 */ { DI_TEXT,      0, wnd_height - 7, 0, wnd_height - 7, 0, nullptr, nullptr, DIF_SEPARATOR },
		/*  3 */ { DI_TEXT,      5, wnd_height - 6, 59, wnd_height - 6, 0, nullptr, nullptr, DIF_NONE, nullptr },
		/*  4 */ { DI_BUTTON,    0, wnd_height - 5, 0, wnd_height - 5, 0, nullptr, nullptr, DIF_CENTERGROUP | DIF_BTNNOCLOSE, _PSI.GetMsg(&_FPG, ps_setup) },
		/*  5 */ { DI_BUTTON,    0, wnd_height - 5, 0, wnd_height - 5, 0, nullptr, nullptr, DIF_CENTERGROUP | DIF_BTNNOCLOSE, _PSI.GetMsg(&_FPG, ps_info) },
		/*  6 */ { DI_BUTTON,    0, wnd_height - 5, 0, wnd_height - 5, 0, nullptr, nullptr, DIF_CENTERGROUP | DIF_BTNNOCLOSE, _PSI.GetMsg(&_FPG, ps_homepage) },
		/*  7 */ { DI_BUTTON,    0, wnd_height - 5, 0, wnd_height - 5, 0, nullptr, nullptr, DIF_CENTERGROUP | DIF_BTNNOCLOSE, _PSI.GetMsg(&_FPG, ps_changelog) },
		/*  8 */ { DI_TEXT,      0, wnd_height - 4, 0, wnd_height - 4, 0, nullptr, nullptr, DIF_SEPARATOR },
		/*  9 */ { DI_BUTTON,    0, wnd_height - 3, 0, wnd_height - 3, 0, nullptr, nullptr, DIF_CENTERGROUP | DIF_BTNNOCLOSE | DIF_NOBRACKETS | DIF_DEFAULTBUTTON, btn_chk_label.c_str() },
		/* 10 */ { DI_BUTTON,    0, wnd_height - 3, 0, wnd_height - 3, 0, nullptr, nullptr, DIF_CENTERGROUP | DIF_BTNNOCLOSE | DIF_NOBRACKETS | DIF_DISABLE, btn_inst_label.c_str() },
		/* 11 */ { DI_BUTTON,    0, wnd_height - 3, 0, wnd_height - 3, 0, nullptr, nullptr, DIF_CENTERGROUP, _PSI.GetMsg(&_FPG, ps_close) }
	};

	_dlg = _PSI.DialogInit(&_FPG, &_FPG, -1, -1, 64, wnd_height, nullptr, dlg_items, sizeof(dlg_items) / sizeof(dlg_items[0]), 0, FDLG_NONE, &update_ui::dlg_proc, this);
	_PSI.DialogRun(_dlg);
	_PSI.DialogFree(_dlg);
}


void update_ui::on_item_selected(const intptr_t idx) const
{
	assert(idx < static_cast<intptr_t>(_mods->size()));
	const module& mod = _mods->at(idx);
	wstring title;
	if (mod.name.length() < DLG_MAX_WIDTH)
		title.resize(DLG_MAX_WIDTH / 2 - mod.name.length() / 2, L' ');
	title += mod.name;
	if (title.length() > DLG_MAX_WIDTH) {
		title.erase(DLG_MAX_WIDTH - 3);
		title += L"...";
	}
	title.resize(DLG_MAX_WIDTH, L' ');
	_PSI.SendDlgMessage(_dlg, DM_SETTEXTPTR, DLGID_SELECT_MOD_TITLE, &title.front());
	_PSI.SendDlgMessage(_dlg, DM_ENABLE, DLGID_SELECT_MOD_HP, reinterpret_cast<void*>(mod.home_page_url.empty() ? FALSE : TRUE));
	_PSI.SendDlgMessage(_dlg, DM_ENABLE, DLGID_SELECT_MOD_CL, reinterpret_cast<void*>(mod.changelog_url.empty() ? FALSE : TRUE));
}


void update_ui::on_module_button(const intptr_t id, const intptr_t idx)
{
	assert(static_cast<size_t>(idx) < _mods->size());

	module& mod = _mods->at(idx);

	switch (id) {
		case DLGID_SELECT_MOD_SU:
			module_setup::edit(mod);
			break;
		case DLGID_SELECT_MOD_IF:
			module_info::view(mod);
			break;
		case DLGID_SELECT_MOD_HP:
			ShellExecute(nullptr, L"open", mod.home_page_url.c_str(), nullptr, nullptr, SW_SHOWNORMAL);
			break;
		case DLGID_SELECT_MOD_CL:
			ShellExecute(nullptr, L"open", mod.changelog_url.c_str(), nullptr, nullptr, SW_SHOWNORMAL);
			break;
		default:
			assert(false && "Unknown id");
	}
}

void update_ui::check_for_updates_worker( std::shared_ptr<UpdaterState> state ) {
	concurrency::parallel_for_each( state->updateItems.begin(), state->updateItems.end(),
																	[ &state ]( std::shared_ptr<UpdateItem> updItem ) {
		net_reader net;
		module& mod = *( updItem->mod );
		state->ui_m.lock();
		{
			std::shared_ptr<UpdateEvent> evt( new UpdateEvent() );
			evt->updItem = updItem;
			evt->eventType = 1;
			state->events.push( evt );
		}
		state->ui_m.unlock();
		state->signaler.signal();

		if ( state->canceled_by_user )
			return;

		// get current item state
		module_update mu( mod, net );
		if ( !mu.check_for_update() ) {
			updItem->update_found = false;

			const wchar_t* err_msg[] = {
				_PSI.GetMsg( &_FPG, ps_title ),
				_PSI.GetMsg( &_FPG, ps_progress_error_check ),
				mod.name.c_str(),
				mu.get_last_error()
			};
			updItem->err_msg = err_msg;
		}

		updItem->can_be_updated = ( mod.ver_offical.valid() || mod.ver_custom.valid() ) && ( mod.ver_offical > mod.ver_current || mod.ver_custom > mod.ver_current );
		state->ui_m.lock();
		{
			std::shared_ptr<UpdateEvent> evt( new UpdateEvent() );
			evt->updItem = updItem;
			evt->eventType = 2;
			state->events.push( evt );
		}
		state->ui_m.unlock();
		state->signaler.signal();
	} );
	state->ui_m.lock();
	{
		std::shared_ptr<UpdateEvent> evt( new UpdateEvent() );
		evt->eventType = 3;
		state->events.push( evt );
	}
	state->ui_m.unlock();
	state->signaler.signal();
}

void update_ui::check_for_updates() {
	bool version_found = false;
	bool updates_found = false;

	const size_t mods_count = _mods->size();

	std::shared_ptr<UpdaterState> state( new UpdaterState() );
	state->canceled_by_user = false;
	for ( size_t i = 0; i < mods_count; ++i ) {
		module& mod = _mods->at( i );

		// if item not checked, we won't check it for new version availability
		FarListGetItem fli;
		ZeroMemory( &fli, sizeof( fli ) );
		fli.StructSize = sizeof( fli );
		fli.ItemIndex = static_cast<intptr_t>( i );
		_PSI.SendDlgMessage( _dlg, DM_LISTGETITEM, DLGID_LIST, &fli );
		if ( ( fli.Item.Flags & LIF_CHECKED ) == 0 ) {
			continue;
		}

		std::shared_ptr<UpdateItem> updItem( new UpdateItem() );
		updItem->index = i;
		updItem->mod = &( _mods->at( i ) );
		updItem->update_found = true;
		updItem->err_msg = nullptr;
		updItem->can_be_updated = false;

		state->updateItems.push_back( updItem );
	}

	progress prg_wnd( _PSI.GetMsg( &_FPG, ps_progress_title_check ), state->updateItems.size() );

	std::thread worker( &update_ui::check_for_updates_worker, this, state );

	bool allDone = false;
	while ( !allDone ) {
		std::queue<std::shared_ptr<UpdateEvent>> events;
		state->signaler.wait();
		state->ui_m.lock();
		events.swap( state->events );
		state->ui_m.unlock();
		while ( !events.empty() ) {
			auto evt = events.front();
			events.pop();

			wstring new_text;
			switch ( evt->eventType ) {
			case 1:
				// just beginning

				// get current data
				FarListGetItem fli;
				ZeroMemory( &fli, sizeof( fli ) );
				fli.StructSize = sizeof( fli );
				fli.ItemIndex = static_cast<intptr_t>( evt->updItem->index );
				_PSI.SendDlgMessage( _dlg, DM_LISTGETITEM, DLGID_LIST, &fli );

				// set to updating state
				FarListUpdate flu;
				ZeroMemory( &flu, sizeof( flu ) );
				flu.StructSize = sizeof( flu );
				flu.Index = fli.ItemIndex;
				flu.Item = fli.Item;
				flu.Item.Flags = LIF_CHECKED | 0x00B7;
				_PSI.SendDlgMessage( _dlg, DM_LISTUPDATE, DLGID_LIST, &flu );

				if ( prg_wnd.aborted() ) {
					state->canceled_by_user = true;
				}
				break;
			case 2:
				// want to update result
				new_text = list_item_text( *( evt->updItem->mod ) );
				FarListUpdate lu;
				ZeroMemory( &lu, sizeof( lu ) );
				lu.StructSize = sizeof( lu );
				lu.Index = static_cast<intptr_t>( evt->updItem->index );
				lu.Item.Text = new_text.c_str();
				lu.Item.Flags = evt->updItem->can_be_updated ? LIF_CHECKED : LIF_GRAYED;
				_PSI.SendDlgMessage( _dlg, DM_LISTUPDATE, DLGID_LIST, &lu );
				// update progress window
				prg_wnd.update( evt->updItem->mod->name.c_str() );
				break;
			case 3:
				allDone = true;
				break;
			default:
				break;
			}
		}
		_PSI.SendDlgMessage( _dlg, DM_REDRAW, 0, nullptr );
	}
	worker.join();

	for ( auto updItem : state->updateItems ) {
		if ( !updItem->update_found )
			_PSI.Message( &_FPG, &_FPG, FMSG_WARNING | FMSG_MB_OK, nullptr, updItem->err_msg, sizeof( updItem->err_msg ) / sizeof( updItem->err_msg[0] ), 0 );

		version_found |= updItem->update_found;
		updates_found |= updItem->can_be_updated;
	}

	if ( version_found ) {
		_PSI.SendDlgMessage( _dlg, DM_ENABLE, DLGID_UPDATE_INSTALL, reinterpret_cast<void*>( TRUE ) );
		if ( updates_found ) {
			FarDialogItem fdi;
			ZeroMemory( &fdi, sizeof( fdi ) );
			_PSI.SendDlgMessage( _dlg, DM_GETDLGITEMSHORT, DLGID_UPDATE_CHECK, &fdi );
			fdi.Flags ^= DIF_DEFAULTBUTTON;
			_PSI.SendDlgMessage( _dlg, DM_SETDLGITEMSHORT, DLGID_UPDATE_CHECK, &fdi );

			//Bug in Far Manager - we can't set new default button (brackets error)
			wstring btn_chk_label = wstring( L"[ " ) + _PSI.GetMsg( &_FPG, ps_check_update ) + wstring( L" ]" );
			wstring btn_inst_label = wstring( L"{ " ) + _PSI.GetMsg( &_FPG, ps_install_update ) + wstring( L" }" );
			_PSI.SendDlgMessage( _dlg, DM_SETTEXTPTR, DLGID_UPDATE_CHECK, &btn_chk_label.front() );
			_PSI.SendDlgMessage( _dlg, DM_SETTEXTPTR, DLGID_UPDATE_INSTALL, &btn_inst_label.front() );

			_PSI.SendDlgMessage( _dlg, DM_SETFOCUS, DLGID_UPDATE_INSTALL, nullptr );
		}
	}

	on_item_selected( 0 );
	_PSI.SendDlgMessage( _dlg, DM_REDRAW, 0, nullptr );
}


bool update_ui::install_updates()
{
	const DWORD afi = another_far_instance();
	if (afi != 0) {
		wchar_t msg[256];
		swprintf_s(msg, _PSI.GetMsg(&_FPG, ps_progress_warn_far), afi);
		if (_PSI.Message(&_FPG, &_FPG, FMSG_WARNING | FMSG_MB_YESNO | FMSG_ALLINONE, nullptr, reinterpret_cast<const wchar_t* const*>(msg), 0, 0) != 1)
			return false;
	}
	if (!check_for_access()) {
		if (_PSI.Message(&_FPG, &_FPG, FMSG_WARNING | FMSG_MB_YESNO | FMSG_ALLINONE, nullptr, reinterpret_cast<const wchar_t* const*>(_PSI.GetMsg(&_FPG, ps_progress_warn_write)), 0, 0) != 1)
			return false;
	}

	//Prepare update info instances
	vector<module*> mods_for_update;
	module* far_mod = nullptr;
	module* self_mod = nullptr;
	for (size_t i = 0; i < _mods->size(); ++i) {
		module& mod = _mods->at(i);

		FarListGetItem fli;
		ZeroMemory(&fli, sizeof(fli));
		fli.StructSize = sizeof(fli);
		fli.ItemIndex = static_cast<intptr_t>(i);
		_PSI.SendDlgMessage(_dlg, DM_LISTGETITEM, DLGID_LIST, &fli);
		if ((fli.Item.Flags & LIF_CHECKED) && (mod.ver_offical.valid() || mod.ver_custom.valid())) {
			if (mod.is_far())
				far_mod = &mod;
			else if (mod.is_self())
				self_mod = &mod;
			else
				mods_for_update.push_back(&mod);
		}
	}
	if (self_mod)
		mods_for_update.push_back(self_mod);
	if (far_mod)
		mods_for_update.push_back(far_mod);

	if (mods_for_update.empty())
		return false;

	//Install updates
	const size_t mods_count = mods_for_update.size();
	progress prg_wnd(_PSI.GetMsg(&_FPG, ps_progress_title_install), mods_count);

	size_t success_update = 0;
	for (size_t i = 0; i < mods_count; ++i) {
		//Update progress window
		prg_wnd.update(mods_for_update[i]->name.c_str());
		if (prg_wnd.aborted())
			return false;

		net_reader net;
		module_update mu(*mods_for_update[i], net);
		if (mu.install_update())
			++success_update;
		else {
			const wchar_t* err_msg[] = { _PSI.GetMsg(&_FPG, ps_title), _PSI.GetMsg(&_FPG, ps_progress_error_install), mods_for_update[i]->name.c_str(), mu.get_last_error() };
			_PSI.Message(&_FPG, &_FPG, FMSG_WARNING | FMSG_MB_OK, nullptr, err_msg, sizeof(err_msg) / sizeof(err_msg[0]), 0);
		}
	}

	settings::set_check_timestamp();

	wchar_t done_msg[64];
	swprintf_s(done_msg, _PSI.GetMsg(&_FPG, ps_progress_done), success_update, mods_count);
	if (!self_mod && !far_mod) {
		const wchar_t* msg[] = { _PSI.GetMsg(&_FPG, ps_progress_title_install), done_msg };
		_PSI.Message(&_FPG, &_FPG, FMSG_MB_OK, nullptr, msg, sizeof(msg) / sizeof(msg[0]), 0);
	}
	else {
		wstring msg = _PSI.GetMsg(&_FPG, ps_progress_title_install);
		msg += L'\n';
		msg += done_msg;
		msg += L'\n';
		msg += _PSI.GetMsg(&_FPG, ps_progress_console);
		if (_PSI.Message(&_FPG, &_FPG, FMSG_ALLINONE, nullptr, reinterpret_cast<const wchar_t* const*>(msg.c_str()), 0, 2) == 1)
			_PSI.AdvControl(&_FPG, ACTL_QUIT, 0, nullptr);
	}

	return true;
}


void update_ui::inverse_selection( const intptr_t idx ) const {
	FarListGetItem fli;
	ZeroMemory( &fli, sizeof( fli ) );
	fli.StructSize = sizeof( fli );
	fli.ItemIndex = idx;
	_PSI.SendDlgMessage( _dlg, DM_LISTGETITEM, DLGID_LIST, &fli );

	FarListUpdate flu;
	ZeroMemory( &flu, sizeof( flu ) );
	flu.StructSize = sizeof( flu );
	flu.Index = fli.ItemIndex;
	flu.Item = fli.Item;
	flu.Item.Flags ^= LIF_CHECKED;
	_PSI.SendDlgMessage( _dlg, DM_LISTUPDATE, DLGID_LIST, &flu );
}


wstring update_ui::list_item_text(const module& mod) const
{
	wstring txt = mod.name;
	if (txt.length() > 25) {
		txt.erase(22);
		txt += L"...";
	}
	txt.resize(25, L' ');
	txt += mod.ver_current.text();
	if (txt.length() < 36)
		txt.resize(36, L' ');
	txt += L" \x1a ";
	txt += mod.ver_offical > mod.ver_custom ? mod.ver_offical.text() : mod.ver_custom.text();
	return txt;
}


DWORD update_ui::another_far_instance() const
{
	DWORD another_far_process = 0;

	const DWORD this_proc_id = GetCurrentProcessId();
	wchar_t this_proc_exe[MAX_PATH];
	if (!GetModuleFileName(nullptr, this_proc_exe, MAX_PATH))
		return 0;

	HANDLE process_snap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (process_snap == INVALID_HANDLE_VALUE)
		return 0;

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(pe32);
	if (Process32First(process_snap, &pe32)) {
		do {
			HANDLE process = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pe32.th32ProcessID);
			if (process != nullptr) {
				wchar_t exe_path[512];
				if (GetModuleFileNameEx(process, NULL, exe_path, sizeof(exe_path) / sizeof(exe_path[0])) &&
					pe32.th32ProcessID != this_proc_id &&
					_wcsicmp(this_proc_exe, exe_path) == 0)
					another_far_process = pe32.th32ProcessID;
				CloseHandle(process);
			}
		}
		while (Process32Next(process_snap, &pe32) && another_far_process == 0);
	}

	CloseHandle(process_snap);

	return another_far_process;
}


bool update_ui::check_for_access() const
{
	wstring path = _PSI.ModuleName;
	path += L".write_check.tmp";

	HANDLE file = CreateFile(path.c_str(), GENERIC_WRITE, FILE_SHARE_READ, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (file == INVALID_HANDLE_VALUE)
		return false;
	CloseHandle(file);

	return DeleteFile(path.c_str()) != FALSE;
}


intptr_t WINAPI update_ui::dlg_proc(HANDLE dlg, intptr_t msg, intptr_t param1, void* param2)
{
	update_ui* instance = nullptr;
	if (msg != DN_INITDIALOG)
		instance = reinterpret_cast<update_ui*>(_PSI.SendDlgMessage(dlg, DM_GETDLGDATA, 0, nullptr));
	else {
		instance = reinterpret_cast<update_ui*>(param2);
		_PSI.SendDlgMessage(dlg, DM_SETDLGDATA, 0, instance);
		instance->on_item_selected(0);
	}
	assert(instance);

	if (msg == DN_LISTCHANGE) {
		instance->on_item_selected(reinterpret_cast<intptr_t>(param2));
		return TRUE;
	}
	else if (msg == DN_BTNCLICK) {
		switch (param1) {
			case DLGID_SELECT_MOD_SU:
			case DLGID_SELECT_MOD_IF:
			case DLGID_SELECT_MOD_HP:
			case DLGID_SELECT_MOD_CL:
				instance->on_module_button(param1, _PSI.SendDlgMessage(dlg, DM_LISTGETCURPOS, DLGID_LIST, nullptr));
				break;
			case DLGID_UPDATE_CHECK:
				instance->check_for_updates();
				break;
			case DLGID_UPDATE_INSTALL:
				if (instance->install_updates())
					_PSI.SendDlgMessage(dlg, DM_CLOSE, 0, nullptr);
				break;
		}
	}
	else if (msg == DN_CONTROLINPUT) {
		const INPUT_RECORD* ir = reinterpret_cast<const INPUT_RECORD*>(param2);
		if (ir->EventType == KEY_EVENT && param1 == DLGID_LIST) {
			if (ir->Event.KeyEvent.wVirtualKeyCode == VK_RETURN) {
				FarDialogItem fdi;
				ZeroMemory(&fdi, sizeof(fdi));
				_PSI.SendDlgMessage(dlg, DM_GETDLGITEMSHORT, DLGID_UPDATE_CHECK, &fdi);
				_PSI.SendDlgMessage(dlg, DN_BTNCLICK, (fdi.Flags & DIF_DEFAULTBUTTON) ? DLGID_UPDATE_CHECK : DLGID_UPDATE_INSTALL, nullptr);
				return TRUE;
			}
			else if (ir->Event.KeyEvent.wVirtualKeyCode == VK_MULTIPLY) {
				//Inverse selection
				for (size_t i = 0; i < instance->_mods->size(); ++i)
					instance->inverse_selection(static_cast<intptr_t>(i));
			}
			else if (ir->Event.KeyEvent.wVirtualKeyCode == VK_SPACE || ir->Event.KeyEvent.wVirtualKeyCode == VK_INSERT) {
				const intptr_t idx = _PSI.SendDlgMessage(dlg, DM_LISTGETCURPOS, DLGID_LIST, nullptr);
				instance->inverse_selection(idx);
 				if (idx < static_cast<int>(instance->_mods->size())) {
					FarListPos flp;
					ZeroMemory(&flp, sizeof(flp));
					flp.StructSize = sizeof(flp);
					flp.TopPos = -1;
					flp.SelectPos = idx + 1;
					_PSI.SendDlgMessage(dlg, DM_LISTSETCURPOS, DLGID_LIST, &flp);
				}
			}
		}
	}

	return _PSI.DefDlgProc(dlg, msg, param1, param2);
}
