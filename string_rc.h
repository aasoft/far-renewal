/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once


//! Plug-in strings
enum plugin_string {
	ps_title,

	ps_save,
	ps_cancel,
	ps_close,
	ps_setup,
	ps_info,
	ps_homepage,
	ps_changelog,
	ps_check_update,
	ps_install_update,

	ps_cfg_check_for_updates,
	ps_cfg_every_day,
	ps_cfg_once_a_week,
	ps_cfg_never,
	ps_cfg_use_proxy,
	ps_cfg_proxy_server,
	ps_cfg_proxy_use_ntlm,
	ps_cfg_proxy_username,
	ps_cfg_proxy_password,

	ps_me_title,
	ps_me_home_page,
	ps_me_changelog,
	ps_me_allow_pr,
	ps_me_allow_nb,
	ps_me_allow_cs,
	ps_me_cs_dl,
	ps_me_cs_rx,
	ps_me_preinst_cmd,
	ps_me_postinst_cmd,
	ps_me_err_dl_empty,
	ps_me_err_rx_error,
	ps_me_err_cmd_empty,

	ps_mi_name,
	ps_mi_description,
	ps_mi_author,
	ps_mi_path,
	ps_mi_guid,
	ps_mi_version,
	ps_mi_state,
	ps_mi_state_active,
	ps_mi_state_unloaded,
	ps_mi_state_cached,

	ps_progress_title_check,
	ps_progress_title_install,
	ps_progress_error_check,
	ps_progress_error_install,
	ps_progress_warn_far,
	ps_progress_warn_write,
	ps_progress_done,
	ps_progress_console,

	ps_autocheck_notify
};
