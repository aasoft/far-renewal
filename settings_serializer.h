#pragma once

#include "common.h"

#define SAVE_SETTINGS(s, n, p) s.set(n, L#p, p);
#define LOAD_SETTINGS(s, n, p) p = s.get(n, L#p, p);


class settings_serializer
{
public:
	settings_serializer()
		: _handle(INVALID_HANDLE_VALUE)
	{
		FarSettingsCreate fsc;
		ZeroMemory(&fsc, sizeof(fsc));
		fsc.StructSize = sizeof(fsc);
		fsc.Guid = _FPG;
		fsc.Handle = INVALID_HANDLE_VALUE;
		if (_PSI.SettingsControl(INVALID_HANDLE_VALUE, SCTL_CREATE, 0, &fsc))
			_handle = fsc.Handle;
	}

	~settings_serializer()
	{
		_PSI.SettingsControl(_handle, SCTL_FREE, 0, nullptr);
	}

	/**
	 * Create section.
	 * \param parent parent section key
	 * \param name section name
	 * \return created section key
	 */
	size_t create_section(const size_t parent, const wchar_t* name) const
	{
		assert(name && *name);

		FarSettingsValue fsv;
		ZeroMemory(&fsv, sizeof(fsv));
		fsv.StructSize = sizeof(fsv);
		fsv.Root = parent;
		fsv.Value = name;
		return static_cast<size_t>(_PSI.SettingsControl(_handle, SCTL_CREATESUBKEY, 0, &fsv));
	}

	/**
	 * Open section.
	 * \param parent parent section key
	 * \param name section name
	 * \return section key
	 */
	size_t open_section(const size_t parent, const wchar_t* name) const
	{
		assert(name && *name);

		FarSettingsValue fsv;
		ZeroMemory(&fsv, sizeof(fsv));
		fsv.StructSize = sizeof(fsv);
		fsv.Root = parent;
		fsv.Value = name;
		return static_cast<size_t>(_PSI.SettingsControl(_handle, SCTL_OPENSUBKEY, 0, &fsv));
	}

	/**
	 * Set value (scalar types).
	 * \param parent parent section key (0 for root)
	 * \param name value name
	 * \param val value
	 * \return false if error
	 */
	template<class T> bool set(const size_t parent, const wchar_t* name, const T& val) const
	{
		assert(name && *name);

		FarSettingsItem fsi;
		ZeroMemory(&fsi, sizeof(fsi));
		fsi.StructSize = sizeof(fsi);
		fsi.Root = parent;
		fsi.Name = name;
		fsi.Type = FST_QWORD;
		fsi.Number = static_cast<unsigned __int64>(val);
		return (_PSI.SettingsControl(_handle, SCTL_SET, 0, &fsi) != FALSE);
	}

	/**
	 * Set value (string type).
	 * \param parent parent section key (0 for root)
	 * \param name value name
	 * \param val value
	 * \return false if error
	 */
	template<> bool set(const size_t parent, const wchar_t* name, const wstring& val) const
	{
		assert(name && *name);

		FarSettingsItem fsi;
		ZeroMemory(&fsi, sizeof(fsi));
		fsi.StructSize = sizeof(fsi);
		fsi.Root = parent;
		fsi.Name = name;
		fsi.Type = FST_STRING;
		fsi.String = val.c_str();
		return (_PSI.SettingsControl(_handle, SCTL_SET, 0, &fsi) != FALSE);
	}

	/**
	 * Set value (boolean type).
	 * \param parent parent section key (0 for root)
	 * \param name value name
	 * \param val value
	 * \return false if error
	 */
	template<> bool set(const size_t parent, const wchar_t* name, const bool& val) const
	{
		return set<unsigned char>(parent, name, val ? 1 : 0);
	}

	/**
	 * Get value (scalar types).
	 * \param parent parent section key (0 for root)
	 * \param name value name
	 * \param default_val default value
	 * \return value
	 */
	template<class T> T get(const size_t parent, const wchar_t* name, const T& default_val) const
	{
		assert(name && *name);

		FarSettingsItem fsi;
		ZeroMemory(&fsi, sizeof(fsi));
		fsi.StructSize = sizeof(fsi);
		fsi.Root = parent;
		fsi.Name = name;
		fsi.Type = FST_QWORD;
		return (_PSI.SettingsControl(_handle, SCTL_GET, 0, &fsi) ? static_cast<T>(fsi.Number) : default_val);
	}

	/**
	 * Get value (string types).
	 * \param parent parent section key (0 for root)
	 * \param name value name
	 * \param default_val default value
	 * \return value
	 */
	template<> wstring get(const size_t parent, const wchar_t* name, const wstring& default_val) const
	{
		assert(name && *name);

		FarSettingsItem fsi;
		ZeroMemory(&fsi, sizeof(fsi));
		fsi.StructSize = sizeof(fsi);
		fsi.Root = parent;
		fsi.Name = name;
		fsi.Type = FST_STRING;
		return (_PSI.SettingsControl(_handle, SCTL_GET, 0, &fsi) ? wstring(fsi.String) : default_val);
	}

	/**
	 * Get value (boolean types).
	 * \param parent parent section key (0 for root)
	 * \param name value name
	 * \param default_val default value
	 * \return value
	 */
	template<> bool get(const size_t parent, const wchar_t* name, const bool& default_val) const
	{
		return get<unsigned char>(parent, name, default_val ? 1 : 0) != 0;
	}

private:
	HANDLE _handle;    ///< Settings handle
};
