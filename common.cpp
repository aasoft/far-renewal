/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "common.h"


wstring common::cp_encode(const string& src, const UINT cp /*= CP_ACP*/)
{
	const int len = static_cast<int>(src.length());
	wstring enc;
	const int req = MultiByteToWideChar(cp, 0, src.c_str(), len, NULL, 0);
	if (req > 0) {
		enc.resize(static_cast<size_t>(req));
		MultiByteToWideChar(cp, 0, src.c_str(), len, &enc.front(), req);
	}
	return enc;
}


string common::cp_encode(const wstring& src, const UINT cp /*= CP_ACP*/)
{
	const int len = static_cast<int>(src.length());

	string enc;
	const int req = WideCharToMultiByte(cp, 0, src.c_str(), len, 0, 0, NULL, NULL);
	if (req > 0) {
		enc.resize(static_cast<size_t>(req));
		WideCharToMultiByte(cp, 0, src.c_str(), len, &enc.front(), req, NULL, NULL);
	}
	return enc;
}


bool common::execute_command(const wchar_t* dir, const wchar_t* cmd, const bool silent, const DWORD wait_time)
{
	assert(dir && *dir);
	assert(cmd && *cmd);

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	si.wShowWindow = silent ? SW_HIDE : SW_SHOW;
	si.dwFlags = STARTF_USESHOWWINDOW;

	static wstring cmd_proc;
	if (cmd_proc.empty()) {
		cmd_proc.resize(MAX_PATH);
		if (!GetSystemDirectory(&cmd_proc.front(), MAX_PATH))
			return false;
		cmd_proc.resize(wcslen(cmd_proc.c_str()));
		cmd_proc += L"\\cmd.exe";
	}

	wstring cmd_line = L"/C ";
	cmd_line += cmd;

	if (!CreateProcess(cmd_proc.c_str(), const_cast<wchar_t*>(cmd_line.c_str()), nullptr, nullptr, FALSE, silent ? CREATE_NO_WINDOW : CREATE_NEW_CONSOLE, nullptr, dir, &si, &pi))
		return false;

	const bool time_out_reached = wait_time && WaitForSingleObject(pi.hProcess, wait_time) == WAIT_TIMEOUT;

	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

	return !time_out_reached;
}
