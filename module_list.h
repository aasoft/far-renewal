/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "common.h"
#include "module.h"
#include "tinyxml/tinyxml.h"


class module_list : public vector<module>
{
private:
	module_list() {}

public:
	/**
	 * Load available modules.
	 * \return modules array
	 */
	static module_list load_modules();

private:
	/**
	 * Load setup xml file.
	 * \param setup xml document
	 * \return false if error
	 */
	bool load_xml(TiXmlDocument& setup) const;

	/**
	 * Load Far Manager module description.
	 */
	void load_far();

	/**
	 * Load plug-in module description.
	 * \param plugin plug-in handle
	 * \param xml_root setup xml root node
	 */
	void load_plugin(HANDLE plugin, const TiXmlElement* xml_root);
};
