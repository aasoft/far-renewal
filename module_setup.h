/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "module.h"


class module_setup
{
private:
	/**
	 * Constructor.
	 * \param mod edited module
	 */
	module_setup(module& mod);

	~module_setup();

public:
	/**
	 * Edit module properties.
	 * \param mod edited module
	 * \return true if module properties was modified
	 */
	static bool edit(module& mod);

private:
	/**
	 * Show UI.
	 * \return true if module properties was modified
	 */
	bool show();

	//Far dialog's callback
	static intptr_t WINAPI dlg_proc(HANDLE dlg, intptr_t msg, intptr_t param1, void* param2);

private:
	module* _mod;   ///< Edited module
	HANDLE  _dlg;   ///< Dialog window handle
};
