/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "module.h"
#include "settings_serializer.h"


module::module(const GUID& g)
: predefined(false),
  loaded(true),
  guid(g),
  far_nightly(false),
  official_permit(true), custom_permit(false),
  preinst_permit(false), postinst_permit(false)
{
}


bool module::load()
{
	settings_serializer ss;
	const size_t section = ss.open_section(0, guid_text().c_str());
	if (section == 0)
		return false;

	LOAD_SETTINGS(ss, section, home_page_url);
	LOAD_SETTINGS(ss, section, changelog_url);
	LOAD_SETTINGS(ss, section, official_permit);
	LOAD_SETTINGS(ss, section, custom_permit);
	LOAD_SETTINGS(ss, section, custom_dlpage);
	LOAD_SETTINGS(ss, section, custom_regexp);
	LOAD_SETTINGS(ss, section, preinst_permit);
	LOAD_SETTINGS(ss, section, preinst_cmd);
	LOAD_SETTINGS(ss, section, postinst_permit);
	LOAD_SETTINGS(ss, section, postinst_cmd);
	if (is_far())
		LOAD_SETTINGS(ss, section, far_nightly);

	return true;
}


void module::save() const
{
	const wstring guid_txt = guid_text();
	settings_serializer ss;
	size_t section = ss.open_section(0, guid_txt.c_str());
	if (section == 0)
		section = ss.create_section(0, guid_txt.c_str());
	if (section == 0)
		return;

	if (!predefined) {
		SAVE_SETTINGS(ss, section, home_page_url);
		SAVE_SETTINGS(ss, section, changelog_url);
		SAVE_SETTINGS(ss, section, official_permit);
		SAVE_SETTINGS(ss, section, custom_permit);
		SAVE_SETTINGS(ss, section, custom_dlpage);
		SAVE_SETTINGS(ss, section, custom_regexp);
	}
	SAVE_SETTINGS(ss, section, preinst_permit);
	SAVE_SETTINGS(ss, section, preinst_cmd);
	SAVE_SETTINGS(ss, section, postinst_permit);
	SAVE_SETTINGS(ss, section, postinst_cmd);
	if (is_far())
		SAVE_SETTINGS(ss, section, far_nightly);
}


wstring module::guid_text() const
{
	LPOLESTR ole_guid = nullptr;
	if (StringFromCLSID(guid, &ole_guid) != S_OK)
		return wstring(L"{?}");
	const wstring guid_txt = ole_guid;
	CoTaskMemFree(ole_guid);
	return guid_txt;
}
