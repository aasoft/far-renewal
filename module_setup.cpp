/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "module_setup.h"
#include "string_rc.h"
#include <regex>

#define DLGID_HOME_PAGE   2
#define DLGID_CHANGELOG   4
#define DLGID_OFFL_PERMIT 6
#define DLGID_FNB_PERMIT  7
#define DLGID_CUST_PERMIT 9
#define DLGID_CUST_DTTLE  10
#define DLGID_CUST_DEDIT  11
#define DLGID_CUST_RTTLE  12
#define DLGID_CUST_REDIT  13
#define DLGID_PREI_PERMIT 15
#define DLGID_PREI_EDIT   16
#define DLGID_POST_PERMIT 17
#define DLGID_POST_EDIT   18
#define DLGID_BTN_CANCEL  21


module_setup::module_setup(module& mod)
: _mod(&mod),
  _dlg(INVALID_HANDLE_VALUE)
{
}


module_setup::~module_setup()
{
}


bool module_setup::edit(module& mod)
{
	module_setup me(mod);
	return me.show();
}


bool module_setup::show()
{
// 	const bool allow_edit = !_mod->predefined && _mod->custom_permit;

	wstring title = _PSI.GetMsg(&_FPG, ps_me_title);
	title += L' ';
	title += _mod->name.c_str();
	title += L" (v.";
	title += _mod->ver_current.text();
	title += L')';

	const FarDialogItem dlg_items[] = {
		/*  0 */ { DI_DOUBLEBOX,  3,  1, 70, 20, 0, nullptr, nullptr, DIF_NONE, title.c_str() },
		/*  1 */ { DI_TEXT,       5,  2, 20,  2, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_me_home_page) },
		/*  2 */ { DI_EDIT,      20,  2, 68,  2, 0, nullptr, nullptr, _mod->predefined ? DIF_READONLY : DIF_NONE, _mod->home_page_url.c_str() },
		/*  3 */ { DI_TEXT,       5,  3, 20,  3, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_me_changelog) },
		/*  4 */ { DI_EDIT,      20,  3, 68,  3, 0, nullptr, nullptr, _mod->predefined ? DIF_READONLY : DIF_NONE, _mod->changelog_url.c_str() },
		/*  5 */ { DI_TEXT,       0,  4,  0,  4, 0, nullptr, nullptr, DIF_SEPARATOR },
		/*  6 */ { DI_CHECKBOX,   5,  5, 68,  5, _mod->official_permit ? 1 : 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_me_allow_pr) },
		/*  7 */ { DI_CHECKBOX,   9,  6, 68,  6, _mod->far_nightly ? 1 : 0, nullptr, nullptr, _mod->is_far() ? DIF_NONE : DIF_DISABLE, _PSI.GetMsg(&_FPG, ps_me_allow_nb) },
		/*  8 */ { DI_TEXT,       0,  7,  0,  7, 0, nullptr, nullptr, DIF_SEPARATOR },
		/*  9 */ { DI_CHECKBOX,   5,  8, 68,  8, _mod->custom_permit ? 1 : 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_me_allow_cs) },
		/* 10 */ { DI_TEXT,       9,  9, 68,  9, 0, nullptr, nullptr, _mod->custom_permit ? DIF_NONE : DIF_DISABLE, _PSI.GetMsg(&_FPG, ps_me_cs_dl) },
		/* 11 */ { DI_EDIT,       9, 10, 68, 10, 0, nullptr, nullptr, !_mod->predefined && _mod->custom_permit ? DIF_NONE : DIF_DISABLE, _mod->custom_dlpage.c_str() },
		/* 12 */ { DI_TEXT,       9, 11, 68, 11, 0, nullptr, nullptr, _mod->custom_permit ? DIF_NONE : DIF_DISABLE, _PSI.GetMsg(&_FPG, ps_me_cs_rx) },
		/* 13 */ { DI_EDIT,       9, 12, 68, 12, 0, nullptr, nullptr, !_mod->predefined && _mod->custom_permit ? DIF_NONE : DIF_DISABLE, _mod->custom_regexp.c_str() },
		/* 14 */ { DI_TEXT,       0, 13, 0,  13, 0, nullptr, nullptr, DIF_SEPARATOR },
		/* 15 */ { DI_CHECKBOX,   5, 14, 68, 14, _mod->preinst_permit ? 1 : 0, nullptr, nullptr, LIF_NONE, _PSI.GetMsg(&_FPG, ps_me_preinst_cmd) },
		/* 16 */ { DI_EDIT,       9, 15, 68, 15, 0, nullptr, nullptr, _mod->preinst_permit ? DIF_NONE : DIF_DISABLE, _mod->preinst_cmd.c_str() },
		/* 17 */ { DI_CHECKBOX,   5, 16, 68, 16, _mod->postinst_permit ? 1 : 0, nullptr, nullptr, LIF_NONE, _PSI.GetMsg(&_FPG, ps_me_postinst_cmd) },
		/* 18 */ { DI_EDIT,       9, 17, 68, 17, 0, nullptr, nullptr, _mod->postinst_permit ? DIF_NONE : DIF_DISABLE, _mod->postinst_cmd.c_str() },
		/* 19 */ { DI_TEXT,       0, 18,  0, 18, 0, nullptr, nullptr, DIF_SEPARATOR },
		/* 20 */ { DI_BUTTON,     0, 19,  0, 19, 0, nullptr, nullptr, DIF_CENTERGROUP | DIF_DEFAULTBUTTON | DIF_FOCUS, _PSI.GetMsg(&_FPG, ps_save) },
		/* 21 */ { DI_BUTTON,     0, 19,  0, 19, 0, nullptr, nullptr, DIF_CENTERGROUP, _PSI.GetMsg(&_FPG, ps_cancel) }
	};

	_dlg = _PSI.DialogInit(&_FPG, &_FPG, -1, -1, 74, 22, nullptr, dlg_items, sizeof(dlg_items) / sizeof(dlg_items[0]), 0, FDLG_NONE, &module_setup::dlg_proc, this);
	const intptr_t rc = _PSI.DialogRun(_dlg);
	const bool modified = rc >= 0 && rc != DLGID_BTN_CANCEL;
	if (modified) {
		_mod->home_page_url = reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(_dlg, DM_GETCONSTTEXTPTR, DLGID_HOME_PAGE, nullptr));
		_mod->changelog_url = reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(_dlg, DM_GETCONSTTEXTPTR, DLGID_CHANGELOG, nullptr));
		_mod->official_permit = _PSI.SendDlgMessage(_dlg, DM_GETCHECK, DLGID_OFFL_PERMIT, nullptr) != 0;
		_mod->far_nightly = _PSI.SendDlgMessage(_dlg, DM_GETCHECK, DLGID_FNB_PERMIT, nullptr) != 0;
		_mod->custom_permit = _PSI.SendDlgMessage(_dlg, DM_GETCHECK, DLGID_CUST_PERMIT, nullptr) != 0;
		_mod->custom_dlpage = reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(_dlg, DM_GETCONSTTEXTPTR, DLGID_CUST_DEDIT, nullptr));
		_mod->custom_regexp = reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(_dlg, DM_GETCONSTTEXTPTR, DLGID_CUST_REDIT, nullptr));
		_mod->preinst_permit = _PSI.SendDlgMessage(_dlg, DM_GETCHECK, DLGID_PREI_PERMIT, nullptr) != 0;
		_mod->preinst_cmd = reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(_dlg, DM_GETCONSTTEXTPTR, DLGID_PREI_EDIT, nullptr));
		_mod->postinst_permit = _PSI.SendDlgMessage(_dlg, DM_GETCHECK, DLGID_POST_PERMIT, nullptr) != 0;
		_mod->postinst_cmd = reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(_dlg, DM_GETCONSTTEXTPTR, DLGID_POST_EDIT, nullptr));
		_mod->save();
	}
	_PSI.DialogFree(_dlg);
	return modified;
}


intptr_t WINAPI module_setup::dlg_proc(HANDLE dlg, intptr_t msg, intptr_t param1, void* param2)
{
	module_setup* instance = nullptr;
	if (msg != DN_INITDIALOG)
		instance = reinterpret_cast<module_setup*>(_PSI.SendDlgMessage(dlg, DM_GETDLGDATA, 0, nullptr));
	else {
		instance = reinterpret_cast<module_setup*>(param2);
		_PSI.SendDlgMessage(dlg, DM_SETDLGDATA, 0, instance);
	}
	assert(instance);

	if (msg == DN_BTNCLICK) {
		switch (param1) {
			case DLGID_OFFL_PERMIT:
				if (instance->_mod->is_far())
					_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_FNB_PERMIT, param2);
				break;
			case DLGID_CUST_PERMIT:
				_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_CUST_DTTLE, reinterpret_cast<void*>(param2));
				_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_CUST_DEDIT, reinterpret_cast<void*>(param2 && !instance->_mod->predefined));
				_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_CUST_RTTLE, reinterpret_cast<void*>(param2));
				_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_CUST_REDIT, reinterpret_cast<void*>(param2 && !instance->_mod->predefined));
				break;
			case DLGID_PREI_PERMIT:
				_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_PREI_EDIT, param2);
				break;
			case DLGID_POST_PERMIT:
				_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_POST_EDIT, param2);
				break;
		}
	}
	else if (msg == DN_CLOSE && param1 >= 0 && param1 != DLGID_BTN_CANCEL) {
		//Validate input
		if (_PSI.SendDlgMessage(dlg, DM_GETCHECK, DLGID_CUST_PERMIT, nullptr)) {
			if (wcslen(reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(dlg, DM_GETCONSTTEXTPTR, DLGID_CUST_DEDIT, nullptr))) == 0) {
				const wchar_t* err_msg[] = { _PSI.GetMsg(&_FPG, ps_title), _PSI.GetMsg(&_FPG, ps_me_err_dl_empty) };
				_PSI.Message(&_FPG, &_FPG, FMSG_WARNING | FMSG_MB_OK, nullptr, err_msg, sizeof(err_msg) / sizeof(err_msg[0]), 0);
				return FALSE;
			}
			const wchar_t* rx = reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(dlg, DM_GETCONSTTEXTPTR, DLGID_CUST_REDIT, nullptr));
			bool rx_err = wcslen(rx) == 0;
			if (!rx_err) {
				try {
					wregex rx_chk(rx);
				}
				catch(regex_error&) {
					rx_err = true;
				}
			}
			if (rx_err) {
				const wchar_t* err_msg[] = { _PSI.GetMsg(&_FPG, ps_title), _PSI.GetMsg(&_FPG, ps_me_err_rx_error) };
				_PSI.Message(&_FPG, &_FPG, FMSG_WARNING | FMSG_MB_OK, nullptr, err_msg, sizeof(err_msg) / sizeof(err_msg[0]), 0);
				return FALSE;
			}
		}
		if ((_PSI.SendDlgMessage(dlg, DM_GETCHECK, DLGID_PREI_PERMIT, nullptr) &&
			wcslen(reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(dlg, DM_GETCONSTTEXTPTR, DLGID_PREI_EDIT, nullptr))) == 0) ||
			(_PSI.SendDlgMessage(dlg, DM_GETCHECK, DLGID_POST_PERMIT, nullptr) &&
			wcslen(reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(dlg, DM_GETCONSTTEXTPTR, DLGID_POST_EDIT, nullptr))) == 0)) {
				const wchar_t* err_msg[] = { _PSI.GetMsg(&_FPG, ps_title), _PSI.GetMsg(&_FPG, ps_me_err_cmd_empty) };
				_PSI.Message(&_FPG, &_FPG, FMSG_WARNING | FMSG_MB_OK, nullptr, err_msg, sizeof(err_msg) / sizeof(err_msg[0]), 0);
				return FALSE;
		}
	}

	return _PSI.DefDlgProc(dlg, msg, param1, param2);
}
