#include "semaphore.h"

void semaphore::signal() {
	std::lock_guard<std::mutex> lock( _mutex );
	++_value;
	if ( _value <= 0 ) {
		++_wakeups;
		_cond.notify_one();
	}
}


void semaphore::wait() {
	std::unique_lock<std::mutex> lock( _mutex );
	--_value;
	if ( _value < 0 ) {
		_cond.wait( lock, [this] { return _wakeups > 0; } );
		--_wakeups;
	}
}