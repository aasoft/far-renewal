/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "net_reader.h"
#include "settings.h"
#include "net_url.h"


net_reader::net_reader()
:	_session(nullptr),
	_connect(nullptr),
	_request(nullptr)
{
}


net_reader::~net_reader()
{
	free();
}


bool net_reader::get_content(const wchar_t* url, vector<unsigned char>& content)
{
	assert(url && *url);

	page_cache::const_iterator it = _cache.find(url);
	if (it != _cache.end()) {
		content = it->second;
		return true;
	}

	free();
	content.clear();

	net_url::components url_comp;
	if (!net_url::parse(url, url_comp)) {
		set_last_error(L"Invalid URL");
		return false;
	}

	HMODULE http_lib = GetModuleHandle(L"Winhttp.dll");
	const bool use_secure = url_comp.scheme.compare(L"https") == 0;

	//Open connection
	_session = WinHttpOpen(L"FarRenewal", settings::use_proxy ? WINHTTP_ACCESS_TYPE_NAMED_PROXY : WINHTTP_ACCESS_TYPE_DEFAULT_PROXY, settings::use_proxy ? settings::proxy_server.c_str() : static_cast<const wchar_t*>(WINHTTP_NO_PROXY_NAME), WINHTTP_NO_PROXY_BYPASS, 0);
	if (!_session) {
		set_last_error(L"WinHttpOpen failed", GetLastError(), http_lib);
		return false;
	}
	_connect = WinHttpConnect(_session, url_comp.host_name.c_str(), url_comp.port, 0);
	if (!_connect) {
		set_last_error(L"WinHttpConnect failed", GetLastError(), http_lib);
		return false;
	}

	//Open request
	const wstring req_path = url_comp.path + url_comp.file + url_comp.query;
	_request = WinHttpOpenRequest(_connect, L"GET", req_path.c_str(), nullptr, WINHTTP_NO_REFERER, WINHTTP_DEFAULT_ACCEPT_TYPES, use_secure ? WINHTTP_FLAG_SECURE : 0);
	if (!_request) {
		set_last_error(L"WinHttpOpenRequest failed", GetLastError(), http_lib);
		return false;
	}

	DWORD req_opt_val;
	if (use_secure) {
		//Disable certificate check
		req_opt_val = SECURITY_FLAG_IGNORE_CERT_CN_INVALID | SECURITY_FLAG_IGNORE_CERT_DATE_INVALID | SECURITY_FLAG_IGNORE_UNKNOWN_CA | SECURITY_FLAG_IGNORE_CERT_WRONG_USAGE;
		WinHttpSetOption(_request, WINHTTP_OPTION_SECURITY_FLAGS, &req_opt_val, sizeof(req_opt_val));
	}

	if (settings::use_proxy) {
		WinHttpSetCredentials(_request, WINHTTP_AUTH_TARGET_PROXY,
			settings::proxy_use_ntlm ? WINHTTP_AUTH_SCHEME_NTLM : WINHTTP_AUTH_SCHEME_BASIC,
			settings::proxy_user.c_str(), settings::proxy_passw.c_str(), nullptr);
	}

	//Enable redirect
	req_opt_val = WINHTTP_OPTION_REDIRECT_POLICY_ALWAYS;
	WinHttpSetOption(_request, WINHTTP_OPTION_REDIRECT_POLICY, &req_opt_val, sizeof(req_opt_val));

	if (!WinHttpAddRequestHeaders(_request, L"Content-Length: 0\n", static_cast<DWORD>(-1L), WINHTTP_ADDREQ_FLAG_ADD)) {
		set_last_error(L"WinHttpAddRequestHeaders failed", GetLastError(), http_lib);
		return false;
	}

	//Send request
	if (!WinHttpSendRequest(_request, WINHTTP_NO_ADDITIONAL_HEADERS, 0, WINHTTP_NO_REQUEST_DATA, 0, 0, 0)) {
		set_last_error(L"WinHttpSendRequest failed", GetLastError(), http_lib);
		return false;
	}

	//Wait for response
	if (!WinHttpReceiveResponse(_request, nullptr)) {
		set_last_error(L"WinHttpReceiveResponse failed", GetLastError(), http_lib);
		return false;
	}

	//Check response status
	DWORD status_val = 0;
	DWORD status_sz = sizeof(DWORD);
	if (WinHttpQueryHeaders(_request, WINHTTP_QUERY_STATUS_CODE | WINHTTP_QUERY_FLAG_NUMBER, NULL, &status_val, &status_sz, NULL)) {
		if (status_val != HTTP_STATUS_OK) {
			wstring resp = L"Bad response (";
			resp += get_code_description(status_val);
			resp +=  L')';
			set_last_error(resp.c_str());
			return false;
		}
	}

	//Get response answer
	DWORD response_size = 0;
	do {
		if (!WinHttpQueryDataAvailable(_request, &response_size)) {
			set_last_error(L"WinHttpQueryDataAvailable failed", GetLastError(), http_lib);
			return false;
		}
		if (!response_size)
			break;
		const size_t curr_size = content.size();
		content.resize(curr_size + response_size);
		DWORD read_size = 0;
		if (!WinHttpReadData(_request, &content[curr_size], response_size, &read_size)) {
			set_last_error(L"WinHttpReadData failed", GetLastError(), http_lib);
			return false;
		}
		if (!read_size)
			break;

	} while (response_size > 0);

	if (content.empty()) {
		set_last_error(L"Empty response from server");
		return false;
	}

	free();

	_cache.insert(make_pair(url, content));

	return true;
}


void net_reader::free()
{
	if (_request) {
		WinHttpCloseHandle(_request);
		_request = nullptr;
	}
	if (_connect) {
		WinHttpCloseHandle(_connect);
		_connect = nullptr;
	}
	if (_session) {
		WinHttpCloseHandle(_session);
		_session = nullptr;
	}
}


wstring net_reader::get_code_description(const int code) const
{
	const wchar_t* descr = NULL;
	switch (code) {
		case HTTP_STATUS_AMBIGUOUS         : descr = L"Server couldn't decide what to return"; break;
		case HTTP_STATUS_MOVED             : descr = L"Object permanently moved"; break;
		case HTTP_STATUS_REDIRECT          : descr = L"Object temporarily moved"; break;
		case HTTP_STATUS_REDIRECT_METHOD   : descr = L"Redirection w/ new access method"; break;
		case HTTP_STATUS_NOT_MODIFIED      : descr = L"If-modified-since was not modified"; break;
		case HTTP_STATUS_USE_PROXY         : descr = L"Redirection to proxy, location header specifies proxy to use"; break;
		case HTTP_STATUS_REDIRECT_KEEP_VERB: descr = L"HTTP/1.1: keep same verb"; break;
		case HTTP_STATUS_BAD_REQUEST       : descr = L"Invalid syntax"; break;
		case HTTP_STATUS_DENIED            : descr = L"Access denied"; break;
		case HTTP_STATUS_PAYMENT_REQ       : descr = L"Payment required"; break;
		case HTTP_STATUS_FORBIDDEN         : descr = L"Request forbidden"; break;
		case HTTP_STATUS_NOT_FOUND         : descr = L"Object not found"; break;
		case HTTP_STATUS_BAD_METHOD        : descr = L"Method is not allowed"; break;
		case HTTP_STATUS_NONE_ACCEPTABLE   : descr = L"No response acceptable to client found"; break;
		case HTTP_STATUS_PROXY_AUTH_REQ    : descr = L"Proxy authentication required"; break;
		case HTTP_STATUS_REQUEST_TIMEOUT   : descr = L"Server timed out waiting for request"; break;
		case HTTP_STATUS_CONFLICT          : descr = L"User should resubmit with more info"; break;
		case HTTP_STATUS_GONE              : descr = L"The resource is no longer available"; break;
		case HTTP_STATUS_LENGTH_REQUIRED   : descr = L"The server refused to accept request w/o a length"; break;
		case HTTP_STATUS_PRECOND_FAILED    : descr = L"Precondition given in request failed"; break;
		case HTTP_STATUS_REQUEST_TOO_LARGE : descr = L"Request entity was too large"; break;
		case HTTP_STATUS_URI_TOO_LONG      : descr = L"Request URI too long"; break;
		case HTTP_STATUS_UNSUPPORTED_MEDIA : descr = L"Unsupported media type"; break;
		case HTTP_STATUS_RETRY_WITH        : descr = L"Retry after doing the appropriate action"; break;
		case HTTP_STATUS_SERVER_ERROR      : descr = L"Internal server error"; break;
		case HTTP_STATUS_NOT_SUPPORTED     : descr = L"Required not supported"; break;
		case HTTP_STATUS_BAD_GATEWAY       : descr = L"Error response received from gateway"; break;
		case HTTP_STATUS_SERVICE_UNAVAIL   : descr = L"Temporarily overloaded"; break;
		case HTTP_STATUS_GATEWAY_TIMEOUT   : descr = L"Timed out waiting for gateway"; break;
		case HTTP_STATUS_VERSION_NOT_SUP   : descr = L"HTTP version not supported"; break;
	}

	wchar_t num[16];
	_itow_s(code, num, 10);
	wstring ret = num;
	if (descr) {
		ret += L' ';
		ret += descr;
	}
	return ret;
}
