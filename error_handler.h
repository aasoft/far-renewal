/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "common.h"


class error_handler
{
public:
	/**
	 * Get last error description.
	 * \return last error description
	 */
	const wchar_t* get_last_error() const;

protected:
	/**
	 * Set last error.
	 * \param description error description
	 * \param err_code system error code
	 * \param module module with error description
	 */
	void set_last_error(const wchar_t* description, const DWORD sys_err_code = 0, const HMODULE module = nullptr);

	/**
	 * Set last error.
	 * \param description1 error description
	 * \param description2 error description
	 * \param err_code system error code
	 * \param module module with error description
	 */
	void set_last_error(const wchar_t* description1, const wchar_t* description2, const DWORD sys_err_code = 0, const HMODULE module = nullptr);

	/**
	 * Set last error.
	 * \param eh another error_handler instance
	 */
	void set_last_error(const error_handler& eh);

private:
	wstring _last_error;   ///< Last error description
};
