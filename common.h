/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include <plugin.hpp>
#include <assert.h>
#include <time.h>
#include <exception>
#include <algorithm>
//#include <sstream>
#include <string>
#include <vector>
//#include <list>
#include <map>

using namespace std;

extern const GUID			_FPG;
extern PluginStartupInfo	_PSI;
extern FarStandardFunctions	_FSF;


namespace common {
	/**
	 * Encoding multi byte to wide string.
	 * \param src source string
	 * \param cp code page
	 * \return wide string
	 */
	wstring cp_encode(const string& src, const UINT cp = CP_ACP);

	/**
	 * Encoding wide to multi byte string
	 * \param src source string
	 * \param cp code page
	 * \return multi byte string
	 */
	string cp_encode(const wstring& src, const UINT cp = CP_ACP);

	/**
	 * Execute command in command processor (cmd.exe).
	 * \param dir current directory
	 * \param cmd command
	 * \param silent silent mode flag
	 * \param wait_time wait time for finish command (0 do disable)
	 * \return false if error
	 */
	bool execute_command(const wchar_t* dir, const wchar_t* cmd, const bool silent, const DWORD wait_time);
};
