/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "module.h"
#include "net_reader.h"
#include "error_handler.h"


class module_update : public error_handler
{
public:
	/**
	 * Constructor.
	 * \param mod updated module
	 * \param reader network reader instance
	 */
	module_update(module& mod, net_reader& reader);

	/**
	 * Get update info from official source (without module data modification).
	 * \param vi output version info
	 * \param dl_link download link to vi version package
	 * \return false if error
	 */
	bool get_update_info(version_info& vi, wstring& dl_link);

	/**
	 * Get update info from custom source (without module data modification).
	 * \param vi output version info
	 * \param dl_link download link to vi version package
	 * \param dl_page custom download page URL
	 * \param regexp custom regular expression to find download link URL
	 * \return false if error
	 */
	bool get_update_info(version_info& vi, wstring& dl_link, const wchar_t* dl_page, const char* regexp);

	/**
	 * Check for update on any available sources.
	 * \return false if error
	 */
	bool check_for_update();

	/**
	 * Install highest version update from on any available sources.
	 * \return false if error
	 */
	bool install_update();

	/**
	 * Unload module.
	 * \return false if error
	 */
	bool unload_module();

	/**
	 * Load module.
	 * \return false if error
	 */
	bool reload_module();

private:
	/**
	 * Construct plugring request URL string.
	 * \return request URL
	 */
	wstring construct_request() const;

	/**
	 * Analyze page for available module version (plugring).
	 * \param page_content page content
	 * \param vi available version info
	 * \param dl_link download link to appropriate module distribution package
	 * \return false if error
	 */
	bool analyze_page(const char* page_content, version_info& vi, wstring& dl_link);

	/**
	 * Analyze page for available module version (custom page).
	 * \param page_content page content
	 * \param dl_page custom download page URL
	 * \param regexp custom regular expression to find download link URL
	 * \param vi available version info
	 * \param dl_link download link to appropriate module distribution package
	 * \return false if error
	 */
	bool analyze_page(const char* page_content, const wchar_t* dl_page, const char* regexp, version_info& vi, wstring& dl_link);

	/**
	 * Update module after restart.
	 * \param old_mod_path path to current module
	 * \param new_mod_path path to new module
	 * \return false if error
	 */
	bool update_restart(const wchar_t* old_mod_path, const wchar_t* new_mod_path);

	/**
	 * Update plug-in.
	 * \param old_mod_path path to current module
	 * \param new_mod_path path to new module
	 * \return false if error
	 */
	bool update_plugin(const wchar_t* old_mod_path, const wchar_t* new_mod_path);

	/**
	 * Find file inside specified directory (recursively).
	 * \param path top directory path
	 * \param file_name file name (nullptr to find any suitable .dll file)
	 * \return path (empty on error)
	 */
	wstring find_file(const wchar_t* path, const wchar_t* file_name) const;

	/**
	 * Create and clear temporary directory.
	 * \param dir_name base directory name
	 * \return temp directory full path (empty on error)
	 */
	wstring prepare_temp_dir(const wchar_t* dir_name);

	/**
	 * Create path tree.
	 * \param path created path
	 * \return false if error
	 */
	bool create_path_tree(const wchar_t* path);

	/**
	 * Recursively remove files and directories.
	 * \param path top path
	 * \return false if error
	 */
	bool delete_path_tree(const wchar_t* path);

	/**
	 * Recursively move files and directories.
	 * \param src source path
	 * \param dst destination path
	 * \return false if error
	 */
	bool move_files(const wchar_t* src, const wchar_t* dst);

private:
	module*     _mod;     ///< Handled module
	net_reader* _reader;  ///< Network reader
};
