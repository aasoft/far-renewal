/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "common.h"


class net_url
{
public:
#ifndef NDEBUG
	/**
	 * URL test function (debug only).
	 */
	static void test();
#endif //NDEBUG

	//! URL components.
	struct components {
		wstring scheme;			//< Scheme name
		wstring host_name;		//< Host name
		unsigned short port;	//< Port number
		wstring path;			//< Path
		wstring file;			//< File
		wstring query;			//< Additional query string
		wstring user_name;		//< User name
		wstring password;		//< Password
	};

	/**
	 * Parse URL.
	 * \param page_url source URL string
	 * \param uc URL components
	 * \return false if source string has incorrect format
	 */
	static bool parse(const wchar_t* page_url, components& uc);

	/**
	 * Collect URL.
	 * \param uc source URL components
	 * \return URL as string
	 */
	static wstring collect(const components& uc);

	/**
	 * Complete URL (relative link to target from parent URL).
	 * \param parent parent URL
	 * \param target target URL
	 * \return full link URL
	 */
	static wstring complete(const wchar_t* parent, const wchar_t* target);
};
