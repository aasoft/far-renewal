/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "common.h"
#include "version_info.h"


struct module
{
	/**
	 * Constructor.
	 * \param g module GUID (GUID_NULL for Far Manager)
	 */
	module(const GUID& g);

	/**
	 * Load module description from settings.
	 * \return false if no settings defined for this module
	 */
	bool load();

	/**
	 * Save module description to settings.
	 * \return
	 */
	void save() const;

	/**
	 * Get modules GUID as text.
	 * \return GUID as text
	 */
	wstring guid_text() const;

	/**
	 * Check for Far Manager module.
	 * \return true if this module is Far Manager
	 */
	bool is_far() const  { return (guid == GUID_NULL) != FALSE; }

	/**
	 * Check for self module (Renewal).
	 * \return true if this module is Renewal
	 */
	bool is_self() const { return (guid == _FPG) != FALSE; }

	//Compare operator (used to sort purpose).
 	bool operator < (const module& other) const
 	{
 		return (is_far() || other.is_far()) ? is_far() : _wcsicmp(name.c_str(), other.name.c_str()) < 0;
 	}


	bool         predefined;       ///< Using predefined values flag
	bool         loaded;           ///< Module state (true=loaded, false=unloaded)

	GUID         guid;             ///< Module GUID (GUID_NULL for Far Manager)
	wstring      name;             ///< Module name
	wstring      path;             ///< Module DLL/EXE path (includes name)
	wstring      description;      ///< Module description
	wstring      author;           ///< Module author
	wstring      home_page_url;    ///< Home web page URL
	wstring      changelog_url;    ///< Change log page URL

	version_info ver_current;      ///< Current version
	version_info ver_offical;      ///< Last available version from official source
	version_info ver_custom;       ///< Last available version from custom source

	bool         far_nightly;      ///< Permit Far manager nightly updates (Far module only)

	bool         official_permit;  ///< Permit update from official source
 	wstring      official_dllink;  ///< Official download link to package URL

	bool         custom_permit;    ///< Permit update from custom source
	wstring      custom_dlpage;    ///< Custom download page URL
	wstring      custom_regexp;    ///< Custom regular expression to find download link URL
 	wstring      custom_dllink;    ///< Custom download link to package

	bool         preinst_permit;   ///< Permit pre-install command execution
	wstring      preinst_cmd;      ///< Pre-install executed command
	bool         postinst_permit;  ///< Permit post-install command execution
	wstring      postinst_cmd;     ///< Post install executed command
};
