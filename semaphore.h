#pragma once

#include <mutex>
#include <condition_variable> 

class semaphore {
private:
	int _value;
	int _wakeups;
	std::mutex _mutex;
	std::condition_variable _cond;

public:
	semaphore( int value ) : _value( value ), _wakeups( 0 ) {}
	semaphore() : semaphore( 0 ) {}

	void wait();
	void signal();
};
