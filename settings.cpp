/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "settings.h"
#include "string_rc.h"
#include "settings_serializer.h"

settings::autochk_period settings::chkperiod = settings::weekly;
time_t settings::last_chk = 0;
bool settings::use_proxy = false;
bool settings::proxy_use_ntlm = false;
wstring settings::proxy_server = L"myproxy:8080";
wstring settings::proxy_user = L"";
wstring settings::proxy_passw = L"";

#define DLGID_CHCKPERIOD 2
#define DLGID_USEPROXY   4
#define DLGID_USENTLM    5
#define DLGID_SRV_T      6
#define DLGID_SRV_E      7
#define DLGID_USR_T      8
#define DLGID_USR_E      9
#define DLGID_PSW_T      10
#define DLGID_PSW_E      11


//! Configuration dialog.
class settings_editor
{
public:
	/**
	 * Show configuration dialog
	 * \return false if canceled
	 */
	bool config()
	{
		FarListItem fli_autocheck[] = {
			{ settings::chkperiod == settings::everyday ? LIF_SELECTED : LIF_NONE, _PSI.GetMsg(&_FPG, ps_cfg_every_day) },
			{ settings::chkperiod == settings::weekly ? LIF_SELECTED : LIF_NONE, _PSI.GetMsg(&_FPG, ps_cfg_once_a_week) },
			{ settings::chkperiod == settings::never ? LIF_SELECTED : LIF_NONE, _PSI.GetMsg(&_FPG, ps_cfg_never) },
		};
		FarList fl_ac;
		ZeroMemory(&fl_ac, sizeof(fl_ac));
		fl_ac.StructSize = sizeof(fl_ac);
		fl_ac.Items = fli_autocheck;
		fl_ac.ItemsNumber = sizeof(fli_autocheck) / sizeof(fli_autocheck[0]);

		const FarDialogItem dlg_items[] = {
			/*  0 */ { DI_DOUBLEBOX,  3,  1, 60, 11, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_title) },
			/*  1 */ { DI_TEXT,       5,  2, 58,  2, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_cfg_check_for_updates) },
			/*  2 */ { DI_COMBOBOX,   5 + 1 + static_cast<intptr_t>(wcslen(_PSI.GetMsg(&_FPG, ps_cfg_check_for_updates))), 2, 58, 2, reinterpret_cast<intptr_t>(&fl_ac), nullptr, nullptr, DIF_DROPDOWNLIST },
			/*  3 */ { DI_TEXT,       0,  3,  0,  3, 0, nullptr, nullptr, DIF_SEPARATOR },
			/*  4 */ { DI_CHECKBOX,   5,  4, 58,  4, settings::use_proxy ? 1 : 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_cfg_use_proxy) },
			/*  5 */ { DI_CHECKBOX,   9,  5, 58,  5, settings::proxy_use_ntlm ? 1 : 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_cfg_proxy_use_ntlm) },
			/*  6 */ { DI_TEXT,       9,  6, 27,  6, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_cfg_proxy_server) },
			/*  7 */ { DI_EDIT,      27,  6, 58,  6, 0, nullptr, nullptr, DIF_NONE, settings::proxy_server.c_str() },
			/*  8 */ { DI_TEXT,       9,  7, 27,  7, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_cfg_proxy_username) },
			/*  9 */ { DI_EDIT,      27,  7, 58,  7, 0, nullptr, nullptr, DIF_NONE, settings::proxy_user.c_str() },
			/* 10 */ { DI_TEXT,       9,  8, 27,  8, 0, nullptr, nullptr, DIF_NONE, _PSI.GetMsg(&_FPG, ps_cfg_proxy_password) },
			/* 11 */ { DI_PSWEDIT,   27,  8, 58,  8, 0, nullptr, nullptr, DIF_NONE, settings::proxy_passw.c_str() },
			/* 12 */ { DI_TEXT,       0,  9,  0,  9, 0, nullptr, nullptr, DIF_SEPARATOR },
			/* 13 */ { DI_BUTTON,     0, 10,  0, 10, 0, nullptr, nullptr, DIF_CENTERGROUP | DIF_DEFAULTBUTTON | DIF_FOCUS, _PSI.GetMsg(&_FPG, ps_save) },
			/* 14 */ { DI_BUTTON,     0, 10,  0, 10, 0, nullptr, nullptr, DIF_CENTERGROUP, _PSI.GetMsg(&_FPG, ps_cancel) }
		};

		HANDLE dlg = _PSI.DialogInit(&_FPG, &_FPG, -1, -1, 64, 13, nullptr, dlg_items, sizeof(dlg_items) / sizeof(dlg_items[0]), 0, FDLG_NONE, &settings_editor::dlg_proc, this);
		const intptr_t rc = _PSI.DialogRun(dlg);
		const bool modified = rc >= 0 && rc != sizeof(dlg_items) / sizeof(dlg_items[0]) - 1;
		if (modified) {
			settings::chkperiod = static_cast<settings::autochk_period>(_PSI.SendDlgMessage(dlg, DM_LISTGETCURPOS, DLGID_CHCKPERIOD, nullptr));
			settings::use_proxy = _PSI.SendDlgMessage(dlg, DM_GETCHECK, DLGID_USEPROXY, nullptr) != 0;
			settings::proxy_use_ntlm = _PSI.SendDlgMessage(dlg, DM_GETCHECK, DLGID_USENTLM, nullptr) != 0;
			settings::proxy_server = reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(dlg, DM_GETCONSTTEXTPTR, DLGID_SRV_E, nullptr));
			settings::proxy_user = reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(dlg, DM_GETCONSTTEXTPTR, DLGID_USR_E, nullptr));
			settings::proxy_passw = reinterpret_cast<const wchar_t*>(_PSI.SendDlgMessage(dlg, DM_GETCONSTTEXTPTR, DLGID_PSW_E, nullptr));
		}
		_PSI.DialogFree(dlg);
		return modified;
	}

private:
	//Far dialog's callback
	static intptr_t WINAPI dlg_proc(HANDLE dlg, intptr_t msg, intptr_t param1, void* param2)
	{
		if (msg == DN_INITDIALOG || (msg == DN_BTNCLICK && param1 == DLGID_USEPROXY)) {
			void* state = reinterpret_cast<void*>(_PSI.SendDlgMessage(dlg, DM_GETCHECK, DLGID_USEPROXY, nullptr));
			_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_USENTLM, state);
			_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_SRV_T, state);
			_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_SRV_E, state);
			_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_USR_T, state);
			_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_USR_E, state);
			_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_PSW_T, state);
			_PSI.SendDlgMessage(dlg, DM_ENABLE, DLGID_PSW_E, state);
		}
		return _PSI.DefDlgProc(dlg, msg, param1, param2);
	}
};


void settings::load()
{
	settings_serializer s;
	LOAD_SETTINGS(s, 0, chkperiod);
	LOAD_SETTINGS(s, 0, last_chk);
	LOAD_SETTINGS(s, 0, use_proxy);
	LOAD_SETTINGS(s, 0, proxy_use_ntlm);
	LOAD_SETTINGS(s, 0, proxy_server);
	LOAD_SETTINGS(s, 0, proxy_user);
	LOAD_SETTINGS(s, 0, proxy_passw);
}


void settings::save()
{
	settings_serializer s;
	SAVE_SETTINGS(s, 0, chkperiod);
	SAVE_SETTINGS(s, 0, last_chk);
	SAVE_SETTINGS(s, 0, use_proxy);
	SAVE_SETTINGS(s, 0, proxy_use_ntlm);
	SAVE_SETTINGS(s, 0, proxy_server);
	SAVE_SETTINGS(s, 0, proxy_user);
	SAVE_SETTINGS(s, 0, proxy_passw);
}


void settings::configure()
{
	settings_editor se;
	if (se.config())
		save();
}


void settings::set_check_timestamp()
{
	last_chk = current_time();
	settings_serializer s;
	SAVE_SETTINGS(s, 0, last_chk);
}


time_t settings::filetime_to_timet(const FILETIME& ft)
{
	ULARGE_INTEGER ull;
	ull.LowPart = ft.dwLowDateTime;
	ull.HighPart = ft.dwHighDateTime;
	return ull.QuadPart / 10000000ULL - 11644473600ULL;
}


FILETIME settings::timet_to_filetime(const time_t& t)
{
	FILETIME ft;
	const LONGLONG ll = Int32x32To64(t, 10000000) + 116444736000000000;
	ft.dwLowDateTime = (DWORD)ll;
	ft.dwHighDateTime = ll >> 32;
	return ft;
}


time_t settings::current_time()
{
	FILETIME ft_current_time;
	SYSTEMTIME st_current_time;
	GetLocalTime(&st_current_time);
	SystemTimeToFileTime(&st_current_time, &ft_current_time);
	return filetime_to_timet(ft_current_time);
}
