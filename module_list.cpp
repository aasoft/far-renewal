/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "module_list.h"

#define RNW_FARMAN_NAME       L"Far Manager"
#define RNW_FARMAN_DESCR      L"File and archive manager"
#define RNW_FARMAN_AUTHOR     L"Eugene Roshal & FAR Group"
#define RNW_FARMAN_HOME_PAGE  L"http://www.farmanager.com"
#define RNW_FARMAN_CHANGELOG  L"http://farmanager.com/svn/trunk/unicode_far/changelog"

#define RNW_SETUP_FILE        L"Renewal.xml"


module_list module_list::load_modules()
{
	module_list mod_lst;

	TiXmlDocument xml_setup;
	const TiXmlElement* xml_root = mod_lst.load_xml(xml_setup) ? xml_setup.RootElement() : nullptr;

	//Load Far manager description
	mod_lst.load_far();

	//Load plug-ins description
	const intptr_t plugs_num = _PSI.PluginsControl(nullptr, PCTL_GETPLUGINS, 0, nullptr);
	if (plugs_num > 0) {
		mod_lst.reserve(mod_lst.size() + plugs_num);
		vector<HANDLE> loaded_plugins(plugs_num);
		if (_PSI.PluginsControl(nullptr, PCTL_GETPLUGINS, static_cast<intptr_t>(plugs_num), &loaded_plugins.front())) {
			for (vector<HANDLE>::const_iterator it = loaded_plugins.begin(); it != loaded_plugins.end(); ++it)
				mod_lst.load_plugin(*it, xml_root);
		}
	}

	//Sort modules by name
	sort(mod_lst.begin(), mod_lst.end());

	return mod_lst;
}


bool module_list::load_xml(TiXmlDocument& setup) const
{
	wstring module_path = _PSI.ModuleName;
	module_path.resize(module_path.rfind(L'\\') + 1);
	module_path += RNW_SETUP_FILE;

	HANDLE file = CreateFile(module_path.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (file == INVALID_HANDLE_VALUE)
		return false;
	LARGE_INTEGER file_size;
	if (!GetFileSizeEx(file, &file_size) || file_size.QuadPart > 1024 * 1024 /* 1MiB */ || file_size.QuadPart == 0) {
		CloseHandle(file);
		return false;
	}
	string xml_text(static_cast<size_t>(file_size.QuadPart), 0);
	DWORD bytes_read = 0;
	if (!ReadFile(file, &xml_text.front(), static_cast<DWORD>(xml_text.size()), &bytes_read, nullptr)) {
		CloseHandle(file);
		return false;
	}
	CloseHandle(file);

	setup.Parse(xml_text.c_str(), nullptr, TIXML_ENCODING_UTF8);
	return !setup.Error();
}


void module_list::load_far()
{
	wchar_t mod_path[MAX_PATH];
	if (!GetModuleFileName(nullptr, mod_path, MAX_PATH))
		return;
	VersionInfo vi;
	if (!_PSI.AdvControl(&_FPG, ACTL_GETFARMANAGERVERSION, 0, &vi))
		return;

	module mod(GUID_NULL);
	mod.ver_current = vi;
	mod.path = mod_path;
	mod.name =          RNW_FARMAN_NAME;
	mod.description =   RNW_FARMAN_DESCR;
	mod.author =        RNW_FARMAN_AUTHOR;
	mod.home_page_url = RNW_FARMAN_HOME_PAGE;
	mod.changelog_url = RNW_FARMAN_CHANGELOG;
	mod.load();
	push_back(mod);
}


void module_list::load_plugin(HANDLE plugin, const TiXmlElement* xml_root)
{
	assert(plugin);

	const intptr_t pi_len = _PSI.PluginsControl(plugin, PCTL_GETPLUGININFORMATION, 0, nullptr);
	if (pi_len <= 0)
		return;
	vector<unsigned char> pi_buff(pi_len);
	FarGetPluginInformation* pi = reinterpret_cast<FarGetPluginInformation*>(&pi_buff.front());
	pi->StructSize = sizeof(FarGetPluginInformation);
	if (!_PSI.PluginsControl(plugin, PCTL_GETPLUGININFORMATION, pi_len, pi))
		return;

	//Check for ignored module list
	if (xml_root) {
		const TiXmlNode* xml_std = xml_root->FirstChild("std");
		if (xml_std) {
			const TiXmlNode* node = NULL;
			while ((node = xml_std->IterateChildren("mod", node)) != NULL) {
				GUID chk_guid;
				const char* chk_guid_txt = node->ToElement()->FirstChild()->Value();
				if (chk_guid_txt && CLSIDFromString(common::cp_encode(chk_guid_txt).c_str(), &chk_guid) == NOERROR && pi->GInfo->Guid == chk_guid)
					return;
			}
		}
	}

	module mod(pi->GInfo->Guid);
	mod.ver_current =   pi->GInfo->Version;
	mod.path =          pi->ModuleName;
	mod.name =          pi->GInfo->Title ? pi->GInfo->Title : wstring();
	mod.description =   pi->GInfo->Description ? pi->GInfo->Description : wstring();
	mod.author =        pi->GInfo->Author ? pi->GInfo->Author : wstring();
	mod.load();

	//Load predefined values
	if (xml_root) {
		const TiXmlNode* xml_pd = xml_root->FirstChild("pd");
		if (xml_pd) {
			const TiXmlNode* node = NULL;
			while ((node = xml_pd->IterateChildren("mod", node)) != NULL) {
				GUID chk_guid;
				const char* chk_guid_txt = node->ToElement()->Attribute("guid");
				if (chk_guid_txt && CLSIDFromString(common::cp_encode(chk_guid_txt).c_str(), &chk_guid) == NOERROR && pi->GInfo->Guid == chk_guid) {
					const TiXmlNode* xml_hp = node->FirstChild("homepage");
					const TiXmlNode* xml_cl = node->FirstChild("changelog");
#ifdef _WIN64
					const TiXmlNode* xml_dl = node->FirstChild("dlpage64");
					const TiXmlNode* xml_rx = node->FirstChild("dlrgex64");
#else
					const TiXmlNode* xml_dl = node->FirstChild("dlpage86");
					const TiXmlNode* xml_rx = node->FirstChild("dlrgex86");
#endif // _WIN64
					if (xml_hp)
						mod.home_page_url = common::cp_encode(xml_hp->FirstChild()->Value());
					if (xml_cl)
						mod.changelog_url = common::cp_encode(xml_cl->FirstChild()->Value());
					if (xml_dl && xml_rx) {
						mod.custom_permit = true;
						mod.custom_dlpage = common::cp_encode(xml_dl->FirstChild()->Value());
						mod.custom_regexp = common::cp_encode(xml_rx->FirstChild()->Value());
					}
					mod.predefined = true;
					break;
				}
			}
		}
	}

	push_back(mod);
}
