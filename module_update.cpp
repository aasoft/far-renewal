/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "module_update.h"
#include "archiver.h"
#include "string_rc.h"
#include "net_url.h"
#include "tinyxml/tinyxml.h"
#include <regex>


#ifdef _WIN64
	#define RNW_FARMAN_DL     L"http://www.farmanager.com/download.php?p=64"
#else
	#define RNW_FARMAN_DL     L"http://www.farmanager.com/download.php?p=32"
#endif //_WIN64
#define RNW_FARMAN_RX_NIGHTLY "nightly/Far(\\d)(\\d)(\\d)?b(\\d+)\\.x\\d\\d\\.\\d+\\.7z"
#define RNW_FARMAN_RX_STABILE "files/Far(\\d)(\\d)(\\d)?b(\\d+)\\.x\\d\\d\\.\\d+\\.7z"

#define RNW_PLUGRING_URL      L"http://plugring.farmanager.com"


module_update::module_update(module& mod, net_reader& reader)
:	_mod(&mod),
	_reader(&reader)
{
}


bool module_update::get_update_info(version_info& vi, wstring& dl_link)
{
	const wstring req_url = construct_request();
	vector<unsigned char> page_content;
	if (!_reader->get_content(req_url.c_str(), page_content)) {
		set_last_error(*_reader);
		return false;
	}
	page_content.push_back(0);	//Last null to use buffer as C string
	return analyze_page(reinterpret_cast<const char*>(&page_content.front()), vi, dl_link);
}


bool module_update::get_update_info(version_info& vi, wstring& dl_link, const wchar_t* dl_page, const char* regexp)
{
	assert(dl_page);
	assert(regexp);

	vector<unsigned char> page_content;
	if (!_reader->get_content(dl_page, page_content)) {
		set_last_error(*_reader);
		return false;
	}
	page_content.push_back(0);	//Last null to use buffer as C string
	return analyze_page(reinterpret_cast<const char*>(&page_content.front()), dl_page, regexp, vi, dl_link);
}


bool module_update::check_for_update()
{
	bool rc = true;

	if (_mod->official_permit) {
		if (_mod->is_far())
			rc = get_update_info(_mod->ver_offical, _mod->official_dllink, RNW_FARMAN_DL, _mod->far_nightly ? RNW_FARMAN_RX_NIGHTLY : RNW_FARMAN_RX_STABILE);
		else
			rc = get_update_info(_mod->ver_offical, _mod->official_dllink);
	}
	if (_mod->custom_permit)
		rc = get_update_info(_mod->ver_custom, _mod->custom_dllink, _mod->custom_dlpage.c_str(), common::cp_encode(_mod->custom_regexp).c_str());

	return rc;
}


bool module_update::install_update()
{
	assert(_mod->ver_offical.valid() || _mod->ver_custom.valid());

	if (!_mod->ver_offical.valid() && !_mod->ver_custom.valid()) {
		set_last_error(L"Update not available");
		return false;
	}

	//Determine highest available version and link
	const wchar_t* dl_link = _mod->ver_offical > _mod->ver_custom ? _mod->official_dllink.c_str() : _mod->custom_dllink.c_str();
	if (dl_link[0] == 0) {
		set_last_error(L"Update not available");
		return false;
	}

	//Download package
	vector<unsigned char> content;
	net_reader nr;
	if (!nr.get_content(dl_link, content)) {
		set_last_error(nr);
		return false;
	}

	//Unpack package to the temporary directory
	const wstring unpack_path = prepare_temp_dir(_mod->name.c_str());
	if (unpack_path.empty())
		return false;
	archiver arch;
	if (!arch.extract(content, unpack_path.c_str())) {
		set_last_error(arch);
		return false;
	}

	//Determine paths
	const wstring old_mod_path(_mod->path.c_str(), _FSF.PointToName(_mod->path.c_str()));
	const wstring new_mod_path = find_file(unpack_path.c_str(), _FSF.PointToName(_mod->path.c_str()));
	if (new_mod_path.empty()) {
		set_last_error(L"Unable to determine new source path");
		return false;
	}

	//Install update
	return (_mod->is_far() || _mod->is_self()) ?
		update_restart(old_mod_path.c_str(), new_mod_path.c_str()) :
		update_plugin(old_mod_path.c_str(), new_mod_path.c_str());
}


bool module_update::unload_module()
{
	if (!_mod->loaded)
		return true;	//Already unloaded

	if (_mod->is_far() || _mod->is_self()) {
		set_last_error(L"Unable to unload Far/self plug-in");
		return false;
	}
	HANDLE far_loaded_plugin = reinterpret_cast<HANDLE>(_PSI.PluginsControl(nullptr, PCTL_FINDPLUGIN, PFM_GUID, &_mod->guid));
	if (far_loaded_plugin && !_PSI.PluginsControl(far_loaded_plugin, PCTL_UNLOADPLUGIN, 0, nullptr)) {
		set_last_error(L"Unable to unload plug-in");
		return false;
	}

	_mod->loaded = false;
	return true;
}


bool module_update::reload_module()
{
	if (_mod->loaded)
		return true;	//Already loaded

	if (!_PSI.PluginsControl(nullptr, PCTL_FORCEDLOADPLUGIN, PLT_PATH, const_cast<wchar_t*>(_mod->path.c_str()))) {
		set_last_error(L"Unable to reload plug-in");
		return false;
	}

	_mod->loaded = true;
	return true;
}


wstring module_update::construct_request() const
{
	LPOLESTR guid_t = nullptr;
	const HRESULT hr = StringFromCLSID(_mod->guid, &guid_t);
	if (hr != S_OK)
		return wstring();
	string guid_text = common::cp_encode(guid_t);
	guid_text.erase(0, 1);
	guid_text.erase(guid_text.length() - 1, 1);
	CoTaskMemFree(guid_t);

	TiXmlDocument xml_doc;
	TiXmlElement* el_plugring = new TiXmlElement("plugring");
	TiXmlElement* el_command = new TiXmlElement("command");
	el_command->SetAttribute("code", "getinfo");
	TiXmlElement* el_uids = new TiXmlElement("uids");
	TiXmlElement* el_uid = new TiXmlElement("uid");
	TiXmlText* uid = new TiXmlText(guid_text.c_str());
	xml_doc.LinkEndChild(el_plugring);
	el_plugring->LinkEndChild(el_command);
	el_plugring->LinkEndChild(el_uids);
	el_uids->LinkEndChild(el_uid);
	el_uid->LinkEndChild(uid);

	TiXmlPrinter printer;
	printer.SetStreamPrinting();
	xml_doc.Accept(&printer);
	wstring req_page = RNW_PLUGRING_URL;
	req_page += L"/command.php?command=";
	req_page += common::cp_encode(printer.CStr());

	return req_page;
}


bool module_update::analyze_page(const char* page_content, version_info& vi, wstring& dl_link)
{
	assert(page_content);
	vi.reset();

	wstring plugring_file_id;

	TiXmlDocument xml_doc;
	xml_doc.Parse(page_content, 0, TIXML_ENCODING_UTF8);
	if (xml_doc.Error()) {
		set_last_error(L"Error parsing response xml");
		return false;
	}
	TiXmlElement* xml_root = xml_doc.RootElement();
	if (!xml_root) {
		set_last_error(L"Error parsing response xml: root node not found");
		return false;
	}
	const TiXmlNode* xml_plugins = xml_root->FirstChild("plugins");
	if (!xml_plugins)
		return true; //Error parsing response xml: plugins node not found
	const TiXmlNode* xml_plugin = xml_plugins->FirstChild("plugin");
	if (!xml_plugin)
		return true; //Error parsing response xml: plugin node not found
	const TiXmlNode* xml_files_t = xml_plugin->FirstChild("files");
	if (!xml_files_t)
		return true; //Error parsing response xml: files node not found
#ifdef _WIN64
	const int platform_flag = 16;
#else
	const int platform_flag = 8;
#endif	//_WIN64
	const TiXmlNode* xml_files = nullptr;
	while ((xml_files = xml_files_t->IterateChildren(xml_files)) != nullptr) {
		const char* flags_txt = xml_files->ToElement()->Attribute("flags");
		if (!flags_txt)
			continue;
		const int flags = atoi(flags_txt);
		if ((flags & platform_flag) == 0)
			continue;

		const char* file_id = xml_files->ToElement()->Attribute("id");
		const char* file_v1 = xml_files->ToElement()->Attribute("version_major");
		const char* file_v2 = xml_files->ToElement()->Attribute("version_minor");
		const char* file_v3 = xml_files->ToElement()->Attribute("version_revision");
		const char* file_v4 = xml_files->ToElement()->Attribute("version_build");
		if (!file_id || !file_v1 || !file_v2 || !file_v3 || !file_v4)
			continue;
		version_info check_ver;
		check_ver.major    = atoi(file_v1);
		check_ver.minor    = atoi(file_v2);
		check_ver.revision = atoi(file_v3);
		check_ver.build    = atoi(file_v4);
		if (check_ver > vi) {
			vi = check_ver;
			plugring_file_id = common::cp_encode(file_id);
		}
	}

	if (!plugring_file_id.empty()) {
		dl_link = RNW_PLUGRING_URL;
		dl_link += L"/download.php?fid=";
		dl_link += plugring_file_id;
	}

	return true;
}


bool module_update::analyze_page(const char* page_content, const wchar_t* dl_page, const char* regexp, version_info& vi, wstring& dl_link)
{
	assert(page_content);
	assert(dl_page);
	assert(regexp);
	vi.reset();

	vector<string> found_links;

	//Analyze page
	try {
		cregex_token_iterator it_invalid;
		regex rx(regexp);
		for (cregex_token_iterator it(page_content, page_content + strlen(page_content), rx); it != it_invalid; ++it)
			found_links.push_back(*it);

		//Find max available version
		for (vector<string>::const_iterator it = found_links.begin(); it != found_links.end(); ++it) {
			smatch res;
			if (regex_search(*it, res, rx)) {
				version_info check_ver;
				check_ver.major    = atoi(res.str(1).c_str());
				check_ver.minor    = atoi(res.str(2).c_str());
				check_ver.revision = atoi(res.str(3).c_str());
				check_ver.build    = atoi(res.str(4).c_str());
				if (check_ver > vi) {
					vi = check_ver;
					dl_link = net_url::complete(dl_page, common::cp_encode(it->c_str()).c_str());
				}
			}
		}
	}
	catch(regex_error&) {
		set_last_error(L"Regular expression error", common::cp_encode(regexp).c_str());
		return false;
	}

	return true;
}


bool module_update::update_restart(const wchar_t* old_mod_path, const wchar_t* new_mod_path)
{
	assert(old_mod_path && *old_mod_path);
	assert(new_mod_path && *new_mod_path);


	wstring script;

	if (_mod->preinst_permit && !_mod->preinst_cmd.empty()) {
		script = _mod->preinst_cmd;
		script += L" && ";
	}
	script += L"update.cmd \"";
	script += new_mod_path;
	script += L"\" \"";
	script += old_mod_path;
	script += L'\"';

	if (_mod->postinst_permit && !_mod->postinst_cmd.empty()) {
		script += L" && ";
		script += L" pushd \"";
		script += old_mod_path;
		script += L"\" && ";
		script += _mod->postinst_cmd;
	}
	script += L" && ";
	script += L" pause";

	wstring module_path = _PSI.ModuleName;
	module_path.resize(module_path.rfind(L'\\') + 1);
	return common::execute_command(module_path.c_str(), script.c_str(), false, 0);
}


bool module_update::update_plugin(const wchar_t* old_mod_path, const wchar_t* new_mod_path)
{
	assert(old_mod_path && *old_mod_path);
	assert(new_mod_path && *new_mod_path);

	//Unload plug-in
	if (!unload_module())
		return false;

	wstring module_path = _mod->path;
	module_path.resize(module_path.rfind(L'\\') + 1);

	if (_mod->preinst_permit && !_mod->preinst_cmd.empty()) {
		if (!common::execute_command(module_path.c_str(), _mod->preinst_cmd.c_str(), false, 60 * 1000)) {
			set_last_error(L"Error executing preinstall command", _mod->preinst_cmd.c_str(), GetLastError());
			return false;
		}
	}

	//Move files
	while (!move_files(new_mod_path, old_mod_path)) {
		wstring msg = _PSI.GetMsg(&_FPG, ps_title);
		msg += L'\n';
		msg += L"Error updating plug-in \"";
		msg += _mod->name.c_str();
		msg += L"\"\n";
		msg += get_last_error();
		if (_PSI.Message(&_FPG, &_FPG, FMSG_ALLINONE | FMSG_WARNING | FMSG_MB_RETRYCANCEL, nullptr, reinterpret_cast<const wchar_t* const*>(msg.c_str()), 0, 0) == 1)
			return false;
	}

	if (_mod->postinst_permit && !_mod->postinst_cmd.empty()) {
		if (!common::execute_command(module_path.c_str(), _mod->postinst_cmd.c_str(), false, 60 * 1000)) {
			set_last_error(L"Error executing postinstall command", _mod->postinst_cmd.c_str(), GetLastError());
			return false;
		}
	}

	//Load plug-in
	reload_module();

	return true;
}


wstring module_update::find_file(const wchar_t* path, const wchar_t* file_name) const
{
	assert(path && *path);

	wstring ret_path;

	wstring find_path(path);
	if (*find_path.rbegin() == L'/' || *find_path.rbegin() == L'\\')
		find_path.erase(find_path.end() - 1);
	find_path += L"\\*";
	WIN32_FIND_DATA find_data;
	HANDLE find_handle = FindFirstFile(find_path.c_str(), &find_data);
	if (find_handle == INVALID_HANDLE_VALUE)
		return wstring();
	do {
		wstring full_path = find_path;
		full_path.erase(full_path.length() - 1);
		if ((find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0) {
			if (file_name) {
				if (_wcsicmp(find_data.cFileName, file_name) == 0)
					ret_path = full_path;
			}
			else {
				const wchar_t* ext = wcsrchr(find_data.cFileName, L'.');
				if (ext && _wcsicmp(ext + 1, L"dll") == 0)
					ret_path = full_path;
			}
		}
		else if (lstrcmp(find_data.cFileName, L".") != 0 && lstrcmp(find_data.cFileName, L"..") != 0) {
			full_path += find_data.cFileName;
			ret_path = find_file(full_path.c_str(), file_name);
		}
	}
	while (FindNextFile(find_handle, &find_data) && ret_path.empty());
	FindClose(find_handle);

	return ret_path;
}


wstring module_update::prepare_temp_dir(const wchar_t* dir_name)
{
	assert(dir_name && *dir_name);

	wchar_t sys_tmp_path[MAX_PATH];
	if (!GetTempPath(MAX_PATH, sys_tmp_path)) {
		set_last_error(L"GetTempPath failed", GetLastError());
		return wstring();
	}

	wstring temp_path = sys_tmp_path;
	temp_path += L"far_renewal\\";
	temp_path += dir_name;
	temp_path += L'\\';
	return delete_path_tree(temp_path.c_str()) && create_path_tree(temp_path.c_str()) ? temp_path : wstring();
}


bool module_update::create_path_tree(const wchar_t* path)
{
	assert(path && *path);

	if (!path || !*path || wcschr(path, L':') == nullptr) {
		set_last_error(path, ERROR_BAD_PATHNAME);
		return false;
	}

	const wstring tree_path(path);
	const wchar_t* delims = L"\\/";
	size_t delim_pos = tree_path.find_first_of(delims);
	while (delim_pos != string::npos) {
		delim_pos = tree_path.find_first_of(delims, delim_pos + 1);
		const wstring dir_path = tree_path.substr(0, delim_pos);
		if (!CreateDirectory(dir_path.c_str(), nullptr)) {
			const DWORD ec = GetLastError();
			if (ec != ERROR_ALREADY_EXISTS) {
				set_last_error(L"Unable to create directory", dir_path.c_str(), ec);
				return false;
			}
		}
	}

	return true;
}


bool module_update::delete_path_tree(const wchar_t* path)
{
	assert(path && *path);

	if (!path || !*path || wcschr(path, L':') == nullptr) {
		set_last_error(path, ERROR_BAD_PATHNAME);
		return false;
	}

	wstring tree_path(path);
	if (*tree_path.rbegin() == L'/' || *tree_path.rbegin() == L'\\')
		tree_path.erase(tree_path.end() - 1);
	wstring find_path = tree_path;
	find_path += L"\\*";

	WIN32_FIND_DATA find_data;
	HANDLE find_handle = FindFirstFile(find_path.c_str(), &find_data);

	if (find_handle == INVALID_HANDLE_VALUE) {
		const DWORD ec = GetLastError();
		if (ec == ERROR_PATH_NOT_FOUND || ec == ERROR_FILE_NOT_FOUND)
			return true;
		set_last_error(L"FindFirstFile failed", find_path.c_str(), ec);
		return false;
	}

	do {
		wstring full_path = tree_path;
		full_path += L'\\';
		full_path += find_data.cFileName;
		if ((find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0) {
			if (!DeleteFile(full_path.c_str())) {
				set_last_error(L"Unable to delete file", full_path.c_str(), GetLastError());
				return false;
			}
		}
		else {
			if (lstrcmp(find_data.cFileName, L".") != 0 && lstrcmp(find_data.cFileName, L"..") != 0 && !delete_path_tree(full_path.c_str()))
				return false;
		}
	}
	while (FindNextFile(find_handle, &find_data));
	FindClose(find_handle);

	if (!RemoveDirectory(tree_path.c_str())) {
		set_last_error(L"Unable to remove directory", tree_path.c_str(), GetLastError());
		return false;
	}

	return true;
}


bool module_update::move_files(const wchar_t* src, const wchar_t* dst)
{
	assert(src && *src);
	assert(GetFileAttributes(src) & FILE_ATTRIBUTE_DIRECTORY);
	assert(dst && *dst);

	wstring find_path = src;
	if (*find_path.rbegin() != L'\\' && *find_path.rbegin() != L'/')
		find_path += L'\\';
	find_path += L'*';

	WIN32_FIND_DATA find_data;
	HANDLE find_handle = FindFirstFile(find_path.c_str(), &find_data);

	if (find_handle == INVALID_HANDLE_VALUE) {
		set_last_error(L"FindFirstFile failed", find_path.c_str(), GetLastError());
		return false;
	}

	do {
		wstring src_path = src;
		if (*src_path.rbegin() != L'\\' && *src_path.rbegin() != L'/')
			src_path += L'\\';
		src_path += find_data.cFileName;

		wstring dst_path = dst;
		if (*dst_path.rbegin() != L'\\' && *dst_path.rbegin() != L'/')
			dst_path += L'\\';
		dst_path += find_data.cFileName;

		if ((find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0) {
			if (!CopyFile(src_path.c_str(), dst_path.c_str(), FALSE)) {
				const DWORD ec = GetLastError();
				wstring msg;
				msg = L"Unable to copy file from\n";
				msg += src_path;
				msg += L"\nto\n";
				msg += dst_path;
				set_last_error(msg.c_str(), ec);
				return false;
			}
			DeleteFile(src_path.c_str());
		}
		else {
			if (lstrcmp(find_data.cFileName, L".") != 0 && lstrcmp(find_data.cFileName, L"..") != 0) {
				if (!CreateDirectory(dst_path.c_str(), NULL)) {
					const DWORD ec = GetLastError();
					if (ec != ERROR_ALREADY_EXISTS) {
						set_last_error(L"Unable to create directory", dst_path.c_str(), ec);
						return false;
					}
				}
				if (!move_files(src_path.c_str(), dst_path.c_str()))
					return false;
				RemoveDirectory(src_path.c_str());
			}
		}
	}
	while (FindNextFile(find_handle, &find_data));
	FindClose(find_handle);

	return true;
}
