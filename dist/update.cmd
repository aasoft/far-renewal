::
:: Script for update
:: Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)

@echo off

set TITLE_OK=Renewal for Far Manager: Installing update
set TITLE_ERR=Renewal for Far Manager: Update error
set TITLE_FIN=Renewal for Far Manager: Success

color 1F
title %TITLE_OK%
echo *********************************************************************
echo %TITLE_OK%
echo *********************************************************************

set SRC_PATH=%1
set DST_PATH=%2
if [%SRC_PATH%] equ [] (
	echo Undefined source path
	title %TITLE_ERR%
	color 4F
	pause
	exit /b 1
)
if [%DST_PATH%] equ [] (
	echo Undefined destination path
	title %TITLE_ERR%
	color 4F
	pause
	exit /b 1
)

echo Please close all Far processes and press any key to install update...
pause > NUL

:copy_try
xcopy /E /Y %SRC_PATH%* %DST_PATH%*
if %ERRORLEVEL% neq 0 (
	color 4F
	title %TITLE_ERR%
	echo Unable to copy files from %SRC_PATH% to %DST_PATH%
	echo Press Ctrl-C to break, any other key to try one more...
	pause > NUL
	color 1F
	goto copy_try
)

color 2F
title %TITLE_FIN%
echo %TITLE_FIN%
rmdir /S /Q %SRC_PATH%
exit /b 0
