/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "net_url.h"


#ifndef NDEBUG
void net_url::test()
{
	components puc1;
	assert(parse(L"http://www.test.host", puc1));
	assert(puc1.scheme == L"http");
	assert(puc1.host_name == L"www.test.host");
	assert(puc1.port == 0);
	assert(puc1.path == L"/");
	assert(puc1.file.empty());
	assert(puc1.query.empty());
	assert(puc1.user_name.empty());
	assert(puc1.password.empty());

	components puc2;
	assert(parse(L"https://www.test.host:8080/path/link?q=1", puc2));
	assert(puc2.scheme == L"https");
	assert(puc2.host_name == L"www.test.host");
	assert(puc2.port == 8080);
	assert(puc2.path == L"/path/");
	assert(puc2.file == L"link");
	assert(puc2.query == L"?q=1");
	assert(puc2.user_name.empty());
	assert(puc2.password.empty());

	components puc3;
	assert(parse(L"https://user_name:passw@www.test.host:443/path/?q=1", puc3));
	assert(puc3.scheme == L"https");
	assert(puc3.host_name == L"www.test.host");
	assert(puc3.port == 443);
	assert(puc3.path == L"/path/");
	assert(puc3.file.empty());
	assert(puc3.query == L"?q=1");
	assert(puc3.user_name == L"user_name");
	assert(puc3.password == L"passw");

	components puc4;
	assert(parse(L"https://www.test.host/plugin/downloads", puc4));
	assert(puc4.scheme == L"https");
	assert(puc4.host_name == L"www.test.host");
	assert(puc4.port == 0);
	assert(puc4.path == L"/plugin/");
	assert(puc4.file == L"downloads");
	assert(puc4.query.empty());
	assert(puc4.user_name.empty());
	assert(puc4.password.empty());

	components cuc1;
	assert(parse(L"http://www.test.host", cuc1));
	assert(collect(cuc1) == L"http://www.test.host");

	components cuc2;
	assert(parse(L"https://www.test.host:8080/path/link?q=1", cuc2));
	assert(collect(cuc2) == L"https://www.test.host:8080/path/link?q=1");

	components cuc3;
	assert(parse(L"http://user_name:passw@www.test.host/path/?q=1", cuc3));
	assert(collect(cuc3) == L"http://user_name:passw@www.test.host/path/?q=1");

	assert(complete(L"http://www.myhost.qq/download/link", L"http://myhost/dl123.zip") == L"http://myhost/dl123.zip");
	assert(complete(L"http://www.myhost.qq/download/link", L"/dl123.zip") == L"http://www.myhost.qq/dl123.zip");
	assert(complete(L"http://www.farmanager.com/download.php?p=32", L"nightly/Far30b2459.x86.20120215.7z") == L"http://www.farmanager.com/nightly/Far30b2459.x86.20120215.7z");
	assert(complete(L"http://code.google.com/p/farplugs/downloads/list", L"//farplugs.googlecode.com/files/FarDiskMenu_3.4_src.zip") == L"http://farplugs.googlecode.com/files/FarDiskMenu_3.4_src.zip");
}
#endif //NDEBUG


bool net_url::parse(const wchar_t* page_url, components& uc)
{
	assert(page_url && *page_url);

	if (!page_url || page_url[0] == 0)
		return false;

	wstring url_parse(page_url);

	//Parse scheme name
	const size_t delim_scheme = url_parse.find(L"://");
	if (delim_scheme == string::npos)
		return false;
	uc.scheme = url_parse.substr(0, delim_scheme);
	transform(uc.scheme.begin(), uc.scheme.end(), uc.scheme.begin(), tolower);
	url_parse.erase(0, delim_scheme + 3);

	//Parse host info
	const size_t delim_host = url_parse.find(L'/');
	wstring host_info;
	if (delim_host == string::npos) {
		host_info = url_parse;
		url_parse.clear();
	}
	else {
		host_info = url_parse.substr(0, delim_host);
		url_parse.erase(0, delim_host);
	}
	if (host_info.empty())
		return false;

	//Parse user name/password
	const size_t delim_login = host_info.rfind(L'@');
	if (delim_login != string::npos) {
		wstring parse_login = host_info.substr(0, delim_login);
		const size_t delim_pwd = parse_login.rfind(L':');
		if (delim_pwd != string::npos) {
			uc.password = parse_login.substr(delim_pwd + 1);
			parse_login.erase(delim_pwd);
		}
		uc.user_name = parse_login;
		host_info.erase(0, delim_login + 1);
	}

	//Parse port
	uc.port = 0;
	const size_t delim_port = host_info.find(L':');
	if (delim_port != string::npos) {
		const wstring port_num = host_info.substr(delim_port + 1);
		uc.port = static_cast<unsigned short>(_wtoi(port_num.c_str()));
		host_info.erase(delim_port);
	}

	//Set host name
	uc.host_name = host_info;
	if (uc.host_name.empty())
		return false;
	transform(uc.host_name.begin(), uc.host_name.end(), uc.host_name.begin(), tolower);

	//Parse path
	const size_t delim_path = url_parse.find(L'?');
	wstring full_path;
	if (delim_path == string::npos) {
		full_path = url_parse;
		url_parse.clear();
	}
	else {
		full_path = url_parse.substr(0, delim_path);
		url_parse.erase(0, delim_path);
	}
	if (!full_path.empty()) {
		const size_t delim_file = full_path.rfind(L'/');
		if (delim_file == string::npos)
			uc.file = full_path;
		else {
			uc.path = full_path.substr(0, delim_file);
			uc.file = full_path.substr(delim_file + 1);
		}
	}
	if (uc.path.empty() || *uc.path.rbegin() != L'/')
		uc.path += L"/";

	//Save query
	uc.query = url_parse;

	return true;
}


wstring net_url::collect(const components& uc)
{
	wstring url;

	url = uc.scheme;
	url += L"://";
	if (!uc.user_name.empty() || !uc.password.empty()) {
		url += uc.user_name;
		url += L':';
		url += uc.password;
		url += L'@';
	}
	url += uc.host_name;
	if (uc.port && !(uc.port == 80 && uc.scheme.compare(L"http") == 0) && !(uc.port == 443 && uc.scheme.compare(L"https") == 0)) {
		url += L':';
		wchar_t num[16];
		_itow_s(uc.port, num, 10);
		url += num;
	}

	if (uc.path.size() != 1 || uc.path[0] != L'/' || !uc.file.empty())
		url += uc.path;
	url += uc.file;
	url += uc.query;

	return url;
}


wstring net_url::complete(const wchar_t* parent, const wchar_t* target)
{
	assert(parent && *parent);
	assert(target && *target);

	if (!*parent || !*target)
		return wstring();

	wstring url;
	wstring tu(target);

	components uc_parent;
	if (!parse(parent, uc_parent))
		return url;

	if (tu.find(L"://") != string::npos) {
		//Absolute path with scheme name specified
		url = target;
	}
	else if(tu.find(L"//") != string::npos) {
		//Absolute path without scheme name specified
		url = uc_parent.scheme + L':';
		url += target;
	}
	else if (tu[0] == L'/') {
		//Link to top folder on host
		url = parent;
		url.erase(url.find(L'/', url.find(L"://") + 4));
		url += target;
	}
	else {
		//Relative link from current path
		uc_parent.path += target;
		uc_parent.file.clear();
		uc_parent.query.clear();
		url = collect(uc_parent);
	}

	return url;
}
