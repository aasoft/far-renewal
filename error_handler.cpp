/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "error_handler.h"


const wchar_t* error_handler::get_last_error() const
{
	return _last_error.c_str();
}


void error_handler::set_last_error(const wchar_t* description, const DWORD sys_err_code /*= 0*/, const HMODULE module /*= nullptr*/)
{
	assert(description || sys_err_code);

	_last_error.clear();

	if (description)
		_last_error = description;

	if (sys_err_code) {
		wchar_t err_num[64];
		swprintf_s(err_num, L"[0x08%X]", sys_err_code);
		if (!_last_error.empty())
			_last_error += L'\n';
		_last_error += err_num;

		wchar_t* err_msg = NULL;
		if (!FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, sys_err_code, 0, reinterpret_cast<LPTSTR>(&err_msg), 512, nullptr) && module)
			FormatMessage(FORMAT_MESSAGE_FROM_HMODULE | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS, module, sys_err_code, 0, reinterpret_cast<LPTSTR>(&err_msg), 512, nullptr);
		if (err_msg) {
			_last_error += L' ';
			_last_error += err_msg;
			LocalFree(err_msg);
		}
		//Remove '\r\n' from the end
		while (!_last_error.empty() && (*_last_error.rbegin() == L'\n' || *_last_error.rbegin() == L'\r' || *_last_error.rbegin() == L' '))
			_last_error.erase(_last_error.end() - 1);
	}
}


void error_handler::set_last_error(const wchar_t* description1, const wchar_t* description2, const DWORD sys_err_code /*= 0*/, const HMODULE module /*= nullptr*/)
{
	assert(description1 && description2);
	wstring descr = description1;
	descr += L'\n';
	descr += description2;
	set_last_error(descr.c_str(), sys_err_code, module);
}


void error_handler::set_last_error(const error_handler& eh)
{
	_last_error = eh._last_error;
}
