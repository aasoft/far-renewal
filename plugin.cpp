/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "common.h"
#include "string_rc.h"
#include "settings.h"
#include "version.h"
#include "update_ui.h"
#include "autocheck.h"


//! Plugin GUID {E3299E7A-1A22-47DD-B270-663BD2B74BCD}
const GUID _FPG = { 0xe3299e7a, 0x1a22, 0x47dd, { 0xb2, 0x70, 0x66, 0x3b, 0xd2, 0xb7, 0x4b, 0xcd } };

PluginStartupInfo    _PSI;
FarStandardFunctions _FSF;


void WINAPI SetStartupInfoW(const PluginStartupInfo* psi)
{
	_PSI = *psi;
	_FSF = *psi->FSF;
	_PSI.FSF = &_FSF;

	settings::load();

	if (settings::chkperiod != settings::never) {
		const time_t last_check_time = settings::last_chk;
		const time_t current_time = settings::current_time();
		const double diff = difftime(current_time, last_check_time);
		const __int64 sec_day = 60 * 60 * 24;
		const __int64 sec_week = sec_day * 7;
		if (diff > (settings::chkperiod == settings::everyday ? sec_day : sec_week))
			start_autocheck();
	}
}


void WINAPI GetGlobalInfoW(GlobalInfo* info)
{
	info->StructSize = sizeof(GlobalInfo);
	info->MinFarVersion = FARMANAGERVERSION;
	info->Version = MAKEFARVERSION(PLUGIN_VERSION_NUM, VS_RELEASE);
	info->Guid = _FPG;
	info->Title = TEXT(PLUGIN_NAME);
	info->Description = TEXT(PLUGIN_DESCR);
	info->Author = TEXT(PLUGIN_AUTHOR);
}


void WINAPI GetPluginInfoW(PluginInfo* info)
{
	assert(info);

	info->StructSize = sizeof(PluginInfo);
	info->Flags |= PF_PRELOAD;

	static const wchar_t* menu_strings[1];
	menu_strings[0] = _PSI.GetMsg(&_FPG, ps_title);
	info->PluginConfig.Guids = &_FPG;
	info->PluginConfig.Strings = menu_strings;
	info->PluginConfig.Count = sizeof(menu_strings) / sizeof(menu_strings[0]);
	info->PluginMenu.Guids = &_FPG;
	info->PluginMenu.Strings = menu_strings;
	info->PluginMenu.Count = sizeof(menu_strings) / sizeof(menu_strings[0]);
}


HANDLE WINAPI OpenW(const OpenInfo*)
{
	stop_autocheck();
	update_ui::run();
	return nullptr;
}


intptr_t WINAPI ConfigureW(const ConfigureInfo*)
{
	stop_autocheck();
	settings::configure();
	return 0;
}


void WINAPI ExitFARW(const ExitInfo*)
{
	stop_autocheck();
}
