/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "autocheck.h"
#include "common.h"
#include "module_list.h"
#include "module_update.h"
#include "string_rc.h"

#define WM_TRAY_TRAYMSG		WM_APP + 0x00001000
#define NOTIFY_DURATION		5000

DWORD  _chk_thread_id = 0;
HANDLE _chk_thread = nullptr;
HANDLE _stop_event = nullptr;


/**
 * Tray window procedure
 */
LRESULT CALLBACK tray_wnd_proc(HWND wnd, UINT msg, WPARAM w_param, LPARAM l_param)
{
	if (msg == WM_TIMER || (msg == WM_TRAY_TRAYMSG && (l_param == WM_LBUTTONDBLCLK || l_param == WM_RBUTTONDOWN)))
		PostQuitMessage(0);
	return DefWindowProc(wnd, msg, w_param, l_param);
}


/**
 * Update checker thread routine
 */
DWORD WINAPI autocheck_proc(LPVOID /*param*/)
{
	HICON tray_icon = NULL;
	HWND tray_wnd = NULL;
	WNDCLASSEX tray_wc;
	NOTIFYICONDATA tray_icondata;
	net_reader net;

	ZeroMemory(&tray_wc, sizeof(tray_wc));
	ZeroMemory(&tray_icondata, sizeof(tray_icondata));

	try {
		//Waiting for Far loaded all plug-ins
		if (WaitForSingleObject(_stop_event, 10000) != WAIT_TIMEOUT)
			throw 0;

		//Check for update for all modules
		module_list mods = module_list::load_modules();
		if (mods.empty())
			throw 0;

		size_t upd_counter = 0;
		for (module_list::iterator it = mods.begin(); it != mods.end(); ++it) {
			if (WaitForSingleObject(_stop_event, 0) != WAIT_TIMEOUT)
				throw 0;
			module_update mu(*it, net);
			if (mu.check_for_update() && (it->ver_offical.valid() || it->ver_custom.valid()) && (it->ver_offical > it->ver_current || it->ver_custom > it->ver_current))
				++upd_counter;
		}
		if (upd_counter == 0)	//No updates found
			throw 0;

		tray_icon = ExtractIcon(GetModuleHandle(nullptr), _PSI.ModuleName, 0);

		tray_wc.cbSize = sizeof(WNDCLASSEX);
		tray_wc.style			= CS_HREDRAW | CS_VREDRAW;
		tray_wc.lpfnWndProc		= &tray_wnd_proc;
		tray_wc.hIcon			= tray_icon;
		tray_wc.hCursor			= LoadCursor(nullptr, IDC_ARROW);
		tray_wc.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);
		tray_wc.lpszClassName	= L"renewal_upnotify_class";
		tray_wc.hIconSm		    = tray_icon;
		if (!RegisterClassEx(&tray_wc))
			throw 0;
		tray_wnd = CreateWindow(tray_wc.lpszClassName, L"", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, nullptr, nullptr);
		if (!tray_wnd)
			throw 0;

		tray_icondata.cbSize = sizeof(NOTIFYICONDATA);
		tray_icondata.uID = 1;
		tray_icondata.hWnd = tray_wnd;
		tray_icondata.uFlags = NIF_ICON | NIF_MESSAGE | NIF_INFO;
		tray_icondata.hIcon = tray_icon;
		tray_icondata.uTimeout = NOTIFY_DURATION;
		tray_icondata.dwInfoFlags = NIIF_INFO | 0x00000020 /*NIIF_LARGE_ICON*/;
		swprintf_s(tray_icondata.szInfo, sizeof(tray_icondata.szInfo) / sizeof(wchar_t), _PSI.GetMsg(&_FPG, ps_autocheck_notify), upd_counter);
		lstrcpy(tray_icondata.szInfoTitle, L"Far Renewal");
		tray_icondata.uCallbackMessage = WM_TRAY_TRAYMSG;

		if (!Shell_NotifyIcon(NIM_ADD, &tray_icondata))
			throw 0;

		SetTimer(tray_wnd, 1, NOTIFY_DURATION, nullptr);

		MSG msg;
		while (GetMessage(&msg, nullptr, 0, 0)) {
			if (msg.message == WM_CLOSE)
				break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	catch (...) {
	}

	KillTimer(tray_wnd, 1);

	if (tray_icondata.cbSize) {
		tray_icondata.uFlags = 0;
		Shell_NotifyIcon(NIM_DELETE, &tray_icondata);
	}
	if (tray_wnd) {
		DestroyWindow(tray_wnd);
		tray_wnd = nullptr;
	}
	if (tray_icon)
		DestroyIcon(tray_icon);
	UnregisterClass(tray_wc.lpszClassName, GetModuleHandle(nullptr));

	CloseHandle(_stop_event);
	_stop_event = nullptr;
	CloseHandle(_chk_thread);
	_chk_thread = nullptr;
	_chk_thread_id = 0;

	return 0;
}


void start_autocheck()
{
	assert(_stop_event == nullptr);

	if (_stop_event == nullptr) {
		_stop_event = CreateEvent(nullptr, FALSE, FALSE, L"renewal_upnotify_stop_event");
		if (!_stop_event || GetLastError() == ERROR_ALREADY_EXISTS)
			return;	//Already started
		_chk_thread = CreateThread(nullptr, 0, &autocheck_proc, nullptr, 0, &_chk_thread_id);
	}
}


void stop_autocheck()
{
	if (_stop_event && _chk_thread && _chk_thread_id) {
		if (_stop_event)
			SetEvent(_stop_event);
		PostThreadMessage(_chk_thread_id, WM_CLOSE, 0, 0);
		if (_chk_thread && WaitForSingleObject(_chk_thread, 3000) == WAIT_TIMEOUT) {
			TerminateThread(_chk_thread, 0);
			CloseHandle(_chk_thread);
			_chk_thread = nullptr;
			_chk_thread_id = 0;
		}
		if (_stop_event) {
			CloseHandle(_stop_event);
			_stop_event = nullptr;
		}
	}
}
