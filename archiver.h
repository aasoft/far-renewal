/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "common.h"
#include "error_handler.h"


class archiver : public error_handler
{
public:
	archiver();
	~archiver();

	/**
	 * Extract files from archive.
	 * \param data archive data
	 * \param dst_path destination path
	 * \return false if error
	 */
	bool extract(const vector<unsigned char>& data, const wchar_t* dst_path);

private:
	/**
	 * Load 7z library dll.
	 * \return false if error
	 */
	bool load_7zlib();

	/**
	 * Get 7z application path from registry.
	 * \param key base parent key
	 * \return path
	 */
	wstring get_7z_path(const HKEY key) const;

private:
	HMODULE _7z_lib; ///< 7zip library handle
	typedef UINT32 (WINAPI* CreateObjectFx)(const GUID* cls_id, const GUID* interface_id, void** out_object);
	CreateObjectFx _create_obj_fx; ///< Function CreateObject from 7zip library
};
