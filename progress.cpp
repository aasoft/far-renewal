/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#include "progress.h"
#include <shlobj.h>

#define PROGRESS_WIDTH 50


progress::progress(const wchar_t* title, const size_t modules_count)
: _title(title),
  _curr_mod(nullptr),
  _curr_module(0),
  _modules_count(modules_count)
{
	_PSI.AdvControl(&_FPG, ACTL_SETPROGRESSSTATE, TBPF_INDETERMINATE, nullptr);
}


progress::~progress()
{
	_PSI.AdvControl(&_FPG, ACTL_PROGRESSNOTIFY, 0, nullptr);
	_PSI.AdvControl(&_FPG, ACTL_SETPROGRESSSTATE, TBPF_NOPROGRESS, nullptr);
	_PSI.PanelControl(PANEL_ACTIVE, FCTL_REDRAWPANEL, 0, nullptr);
	_PSI.PanelControl(PANEL_PASSIVE, FCTL_REDRAWPANEL, 0, nullptr);
}


void progress::update(const wchar_t* current_mod_name /*= nullptr*/)
{
	assert(_curr_module < _modules_count);

	if (current_mod_name)
		_curr_mod = current_mod_name;

	assert(_curr_mod);

	const size_t percent = static_cast<size_t>((_curr_module * 100) / _modules_count);
	assert(percent <= 100);

	ProgressValue pv;
	ZeroMemory(&pv, sizeof(pv));
	pv.StructSize = sizeof(pv);
	pv.Completed = percent;
	pv.Total = 100;
	_PSI.AdvControl(&_FPG, ACTL_SETPROGRESSVALUE, 0, &pv);

	wstring bar(PROGRESS_WIDTH, L'\x2591');
	const size_t fill_length = percent * bar.size() / 100;
	fill(bar.begin(), bar.begin() + fill_length, L'\x2588');

	const wchar_t* msg[] = { _title, bar.c_str(), _curr_mod };
	_PSI.Message(&_FPG, &_FPG, FMSG_NONE, nullptr, msg, sizeof(msg) / sizeof(msg[0]), 0);

	if (current_mod_name)
		++_curr_module;
}


bool progress::aborted()
{
	HANDLE std_in = GetStdHandle(STD_INPUT_HANDLE);
	INPUT_RECORD rec;
	DWORD read_count = 0;
	while (PeekConsoleInput(std_in, &rec, 1, &read_count) && read_count != 0) {
		ReadConsoleInput(std_in, &rec, 1, &read_count);
		if (rec.EventType == KEY_EVENT && rec.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE && rec.Event.KeyEvent.bKeyDown)
			return true;
	}
	return false;
}
