/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "module_list.h"
#include <TlHelp32.h>
#include <ppl.h>
#include <memory>
#include <thread>
#include <mutex>
#include <queue>
#include <Psapi.h>
#include "semaphore.h"


class update_ui
{
private:
	update_ui(module_list* mods);
	~update_ui();

public:
	/**
	 * Run update UI.
	 */
	static void run();

private:
	/**
	 * Show update UI.
	 */
	void show();

	void on_item_selected(const intptr_t idx) const;

	void on_module_button(const intptr_t id, const intptr_t idx);
	void check_for_updates();
	bool install_updates();

	/**
	 * Inverse selection for list item.
	 * \param idx list item index
	 */
	void inverse_selection(const intptr_t idx) const;

	/**
	 * Create list item text.
	 * \param mod module
	 * \return list item text
	 */
	wstring list_item_text(const module& mod) const;

	/**
	 * Get another Far process instance id.
	 * \return another Far process instance id or 0 if not found
	 */
	DWORD another_far_instance() const;

	/**
	 * Check for write access to plug-ins folder.
	 * \return false if error
	 */
	bool check_for_access() const;

	//Far dialog's callback
	static intptr_t WINAPI dlg_proc(HANDLE dlg, intptr_t msg, intptr_t param1, void* param2);

	struct UpdateItem {
	public:
		size_t index;
		module *mod;
		bool update_found;
		bool can_be_updated;
		const wchar_t** err_msg;
	};
	
	struct UpdateEvent {
		std::shared_ptr<UpdateItem> updItem;
		char eventType;
	};
	struct UpdaterState {
	public:
		vector<std::shared_ptr<UpdateItem>> updateItems;
		std::mutex ui_m;
		semaphore signaler;
		bool canceled_by_user;
		std::queue<std::shared_ptr<UpdateEvent>> events;
	};

	void check_for_updates_worker( std::shared_ptr<UpdaterState> state );


private:
	module_list* _mods;  ///< Modules list
	HANDLE       _dlg;   ///< Dialog window handle
};
