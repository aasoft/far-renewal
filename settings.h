/**************************************************************************
 *  Renewal plug-in for FAR 3.0 (http://code.google.com/p/farplugs)       *
 *  Copyright (C) 2012-2013 by Artem Senichev <artemsen@gmail.com>        *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 **************************************************************************/

#pragma once

#include "common.h"


class settings
{
public:
	/**
	 * Load settings.
	 */
	static void load();

	/**
	 * Configure settings.
	 */
	static void configure();

	/**
	 * Set last check timestamp.
	 */
	static void set_check_timestamp();

	/**
	 * Convert FILETIME to time_t.
	 * \param ft source FILETIME value
	 * \return time_t value
	 */
	static time_t filetime_to_timet(const FILETIME& ft);

	/**
	 * Convert time_t to FILETIME.
	 * \param t source time_t value
	 * \return FILETIME value
	 */
	static FILETIME timet_to_filetime(const time_t& t);

	/**
	 * Get current time.
	 * \return time_t value
	 */
	static time_t current_time();

private:
	/**
	 * Save settings.
	 */
	static void save();

public:
	//! Check for autoupdate period.
	enum autochk_period {
		everyday,
		weekly,
		never
	};

	static autochk_period chkperiod;	///< Auto check for update period
	static time_t last_chk;				///< Last autocheck datetime stamp
	static bool use_proxy;				///< Proxy using flag
	static bool proxy_use_ntlm;			///< Using NTLM authentication scheme for proxy
	static wstring proxy_server;		///< Proxy server
	static wstring proxy_user;			///< Proxy user name
	static wstring proxy_passw;			///< Proxy password
};
